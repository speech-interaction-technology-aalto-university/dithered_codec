clear

gainfn = @(x,ref) (x(:)'*ref(:))/(x(:)'*x(:));
normedfn = @(x,ref) x*gainfn(x,ref);


N = 100000;

x = randl(N,1); x = x/std(x);
r = rand(N,1);
dvec = 2.^linspace(-10,4,40)';

e2vec = zeros(numel(dvec),1);
b = zeros(numel(dvec),1);
g = zeros(numel(dvec),1);
s = zeros(numel(dvec),1);
c = sqrt(.5);

for dix = 1:numel(dvec)
    d = dvec(dix);
    
    xq = d*(0.5 + r + floor(x/d - r));
    xl = d*(0.0 + r + floor(x/d - r));
    xu = d*(1.0 + r + floor(x/d - r));
    p = laplacian_cdf(xu)-laplacian_cdf(xl);
    
    xh = (c^2./p).*(-exp(-abs(xu)/c).*(abs(xu)+c) + exp(-abs(xl)/c).*(abs(xl)+c));
    
    b(dix) = -mean(log2(p));
    e = x-xh;
    e2vec(dix) = var(e);    
    
    g(dix) = gainfn(xq,x);
    s(dix) = std(xq);
   
    if 0
        clf
        plot([x(1:100) xq(1:100) xh(1:100)])
        %ax = axis;
        %hold on
        %plot([ xh(1:100)])
        %axis(ax)
        title(num2str(20*log10(norm(x)./[norm(x-xq) norm(x-xh)])))
        waitforbuttonpress
    end
end

clf

softplus = @(x) log(exp(x)+1);

subplot(221)
bh = 1.9427-log2(dvec);
bh2 = log(exp(bh)+1);
%bh2 = log((  (dvec.\(2*exp(1))).^(1/log(2))   )+1);
plot(-log2(dvec),[b bh bh2])
hold on
plot(-log2(dvec([1 end])),[1 1],'k--')
grid on

subplot(222)
plot(-log2(dvec), [g laplacian_cdf(1.3-sqrt(.5)*log2(dvec))])
title('g')
grid on

subplot(223)
plot(-log2(dvec), [s 1+2*softplus(log2(dvec)*1.5-4.3)])
title('s')
grid on

subplot(224)
plot(-log2(dvec), [-log2(e2vec) -log2(dvec.^2/12) 2*softplus(-.5*log2(dvec.^2/12))])
title('s')
grid on

