function [xq,snr] = dithered_codec(x,codec_mode,runtime_mode,EnvStat)


        winnum = floor((length(x)-codec_mode.window_length)/codec_mode.window_step)+1;

        xq = zeros(size(x));
        xq0 = xq;
        bits_vec = zeros(winnum,1);    
        Xmat = zeros(codec_mode.speclen,winnum);
        Xqmat = Xmat;
        for k=1:winnum
            ix = (k-1)*codec_mode.window_step + (1:codec_mode.window_length);
            xwin = x(ix).*codec_mode.hwin;

            X = codec_mode.MdctMtx*xwin;
            Xmat(:,k) = X;


            %% Envelope 
            Env = codec_mode.EnvFn(X);

            % quantize
            Ql = floor(Env/EnvStat.dq_env)*EnvStat.dq_env;
            Qu = ceil(Env/EnvStat.dq_env)*EnvStat.dq_env;

            % encode and decode
            [bits_env,Envq] = envelope_coder(Ql,Qu,EnvStat,codec_mode,runtime_mode.bits_per_frame);
            bits_remaining = runtime_mode.bits_per_frame - bits_env;

            % Reconstructed envelop (std of signal)
            Xstd = diag(EnvStat.specstd)*(10.^((codec_mode.iLogEnvFn(Envq))/20));        

            %% Perceptual model     
            W = perceptual_model(Xstd,codec_mode);        
            %W(:) = 1;


            if 0 % codec_mode.encoder_F0_model
                d = dct(abs(X./Xstd));
                [~,T0h] = max(d(1+(EnvStat.F0postfilter_T0range(1):EnvStat.F0postfilter_T0range(2))));
                %T0 = T0h+EnvStat.F0postfilter_T0range(1);

                Xstd = Xstd.*EnvStat.F0postfilter(:,T0h);
                T0count = (EnvStat.F0postfilter_T0range(2)-EnvStat.F0postfilter_T0range(1)+1);
                bits_remaining = bits_remaining - log2(T0count);

                if 1
                    clf
                    Xstd0 = Xstd./EnvStat.F0postfilter(:,T0h);
                    plot(20*log10(abs([X Xstd0 Xstd])))
                    waitforbuttonpress
                end
            end        


            %% Perceptual relative entropy
            Wl = log2(Xstd./W); % log2 of weigthed spectrum is equal to relative bitrate

            %% Quantization and coding of residual/whitened spectrum
            Xw = X./Xstd;
            Xwq = spectrum_coder(Xw,bits_remaining,codec_mode,Wl,EnvStat);


            %% resynthesis
            Xq = Xwq.*Xstd; % restore envelope
            Xqmat(:,k) = Xq;
            
            xwinq = 2*codec_mode.MdctMtx'*Xq;
            ix = (k-1)*codec_mode.window_step + (1:codec_mode.window_length);
            xq(ix) = xq(ix) + xwinq.*codec_mode.hwin;

            
            %% evaluation
            if nargout > 1
                W_oracle = perceptual_model(abs(X),codec_mode);        
                snr(k,1) = 20*log10(norm(X./sqrt(W_oracle))./norm((X-Xq)./sqrt(W_oracle)));
            end

            if sum(isfinite(xq+xq0)) ~= numel(xq)
                warning('To infinity and beyond.')
            end

            %% plotting
            if 0
                clf
                subplot(211)
                plot(20*log10(abs([Xstd/max(Xstd) W oracle_X])))
                subplot(212)
                plot(Wl)
                waitforbuttonpress
            end

        end

        
    if 0
        clf
        plot([x xq])
    elseif 0
        plot(bar)
        waitforbuttonpress
    elseif 0
        clf
        subplot(211)
        surf(20*log10(abs(Xmat)+0.001),'edgecolor','none')
        view(2)
        axis tight
        subplot(212)
        surf(20*log10(abs(Xqmat)+0.001),'edgecolor','none')
        view(2)
        axis tight        
        drawnow
    elseif 0
        clf
        subplot(211)
        surf(20*log10(abs(Xqmat)+0.001),'edgecolor','none')
        view(2)
        axis tight
        subplot(212)
        plot(snr)
    end
        