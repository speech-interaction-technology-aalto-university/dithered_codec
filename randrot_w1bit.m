function xh = randrot_w1bit(x,lw)
% function xh = randrot_w1bit(x,lw)
% Quantize x with less than 1 bit per sample, such that number of bits per
% sample is defined by lw, assuming Gaussian univariance input


N = length(x);

% sort to ascending order
[lw,six] = sort(lw);
x = x(six);

lw = max(0,lw);
lw = min(1,lw);

sumbits = sum(lw);
offsetbits = sumbits - round(sumbits);
while abs(offsetbits) > 0.001    
    lw = lw - offsetbits/N;
    lw = max(0,lw);
    lw = min(1,lw);

    sumbits = sum(lw);
    offsetbits = sumbits - round(sumbits);
end    


% when quantizing a univariance Gaussian with a 1bit quantizer, the optimal
% formula is xh = sqrt(2/pi)*sign(x) and the mean square error is 1-2/pi.
if 0 % verify this argument
    gainfn = @(x,ref) (x(:)'*ref(:))/(x(:)'*x(:));
    normedfn = @(x,ref) x*gainfn(x,ref);
    
    x = randn(100000,1);
    xh = sign(x);
    g = gainfn(xh,x);
    [g sqrt(2/pi)]
    
    [1-2/pi mean((x-sqrt(2/pi)*xh).^2)]
end
% Error variance with 0 bits is 1 and with 1 bits its 1-2/pi. 
t_e2 = 1-lw*2/pi; % target error variance

% quantizer error variance
q_e2 = [ones(N-round(sumbits),1); ones(round(sumbits),1)*(1-2/pi)];

P = eye(N);
k = N;
h = 1;
while h < k
    while q_e2(h) <= t_e2(h)
        h = h+1;
        if h == k, break, end
    end
    if h == k, break, end

    foo1 = (t_e2(k)-q_e2(h))/(q_e2(k)-q_e2(h));
    foo2 = (t_e2(h)-q_e2(k))/(q_e2(h)-q_e2(k));
    if foo1 > foo2
        foo = foo1;
        if (foo > 0) && (foo < 1)
            cos_t = sqrt(foo);
            sin_t = sqrt(1-cos_t^2);

            Pix = [cos_t sin_t; -sin_t cos_t];

            P([k h],:) = Px*P([k h],:);
            q_e2([k h]) = diag(Pix*diag(q_e2([k h]))*Pix');

            %plot([t_e2 q_e2])
            %waitforbuttonpress

            k = k-1;

        else
            h = h+1;
        end    
    else
        foo = foo2;
        if (foo > 0) && (foo < 1)
            cos_t = sqrt(foo);
            sin_t = sqrt(1-cos_t^2);

            Pix = [cos_t sin_t; -sin_t cos_t];

            P([h k],:) = Px*P([h k],:);
            q_e2([h k]) = diag(Pix*diag(q_e2([h k]))*Pix');

            %plot([t_e2 q_e2])
            %waitforbuttonpress

            h = h+1;

        else
            k = k-1;
        end    
    end
end

y = P*x;
yh = [zeros(N-round(sumbits),1); sign(y((N-round(sumbits)+1):N))*sqrt(2/pi)]; 
xh = P'*yh;
%plot([t_e2 q_e2])

% restort sorting
xh(six) = xh;