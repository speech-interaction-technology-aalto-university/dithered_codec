clear

% default configuration
[codec_mode,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters();
[d_train,d_test,filenum_train,filenum_test] = extract_filenames(timit_path,temp_path);

x=readsph(d_test{1},[],-1,-1);

clf
colormap default
subplot(321)
plot_single_spectrogram(x,codec_mode)
set(gca,'Fontname','times','fontsize',16,'YTick',[],'XTickLabel',[])
title('Original')

subplot(322)
plot_spectra({'dithering', false; 'noise_fill', false; 'quantizer_1bit', false})
set(gca,'Fontname','times','fontsize',16,'YTick',[],'XTickLabel',[])
title('Dithering: No, Randomization: No, Noise-fill: No')

subplot(323)
plot_spectra({'dithering', true; 'noise_fill', false; 'quantizer_1bit', false})
set(gca,'Fontname','times','fontsize',16,'YTick',[],'XTickLabel',[])
title('Dithering: Yes, Randomization: No, Noise-fill: No')

subplot(324)
plot_spectra({'dithering', true; 'noise_fill', false; 'quantizer_1bit', true})
set(gca,'Fontname','times','fontsize',16,'YTick',[],'XTickLabel',[])
title('Dithering: Yes, Randomization: Yes, Noise-fill: No')

subplot(325)
plot_spectra({'dithering', false; 'noise_fill', true; 'quantizer_1bit', false})
set(gca,'Fontname','times','fontsize',16,'YTick',[],'XTickLabel',[])
title('Dithering: No, Randomization: No, Noise-fill: Yes')

subplot(326)
plot_spectra({'dithering', true; 'noise_fill', true; 'quantizer_1bit', true})
set(gca,'Fontname','times','fontsize',16,'YTick',[],'XTickLabel',[])
title('Dithering: Yes, Randomization: Yes, Noise-fill: Yes')


print -deps2c dithering.eps
!epstopdf dithering.eps
