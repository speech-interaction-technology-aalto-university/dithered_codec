function train_all(codec_mode,force)

if nargin < 1
    codec_mode = generate_parameters();
end
if nargin < 2
    force = 0;
end


[~,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters();
if length(dir(temp_path)) == 0
    eval(['!mkdir ' temp_path])
end

generate_dataset(codec_mode)

train_envelope_mixture(codec_mode,force)

train_envelope_model(codec_mode,force)

train_spectral_bias(codec_mode,force)

