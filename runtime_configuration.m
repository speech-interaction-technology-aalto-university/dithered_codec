function runtime_mode = runtime_configuration(codec_mode,paramlist)


runtime_mode = struct();

% defaults
runtime_mode.bitrate_per_s = 13200;



if nargin > 1
    for ix = 1:size(paramlist,1)
        switch lower(paramlist{ix,1})
            case 'bitrate'
                runtime_mode.bitrate_per_s = paramlist{ix,2};
                ix = ix+1;
            otherwise
                error(['Unknown parameter ' paramlist{ix,1}])
        end
    end
end


%% Bitrate
frame_per_s = codec_mode.sampling_frequency/codec_mode.window_step;
runtime_mode.bits_per_frame = runtime_mode.bitrate_per_s/frame_per_s;
