function x = plot_results(parameter_list)

if length(dbstack) == 1
    disp('Display results for dithered codec')
end

parameter_count = size(parameter_list,1);

% default configuration
[codec_mode,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters();


for ix=1:parameter_count
    if strcmp(class(parameter_list{ix,2}),'char')
        this_argnum = 1;
    else
        this_argnum = length(parameter_list{ix,2});
    end
   if this_argnum == 1
       s = generate_evaluation_file_name(temp_path,parameter_list);
       load(s,'snrvector','snr_file_list')
       x = struct('snrvector',snrvector,'snr_file_list',snr_file_list);
       return
   else
       %x = struct();
       for k=1:this_argnum
           parameter_list_excerpt = parameter_list;
           switch class(parameter_list{ix,2})
               case {'double','logical'}
                   parameter_list_excerpt{ix,2} = parameter_list{ix,2}(k);
               case 'cell'
                   parameter_list_excerpt{ix,2} = parameter_list{ix,2}{k};
           end
           x(k) = plot_results(parameter_list_excerpt);
       end
   end
end


figure(1)
clf
for k=1:this_argnum
    [h,hix] = hist(x(k).snrvector,30);
    plot(hix,h,'linewidth',2)
    hold on
    switch class(parameter_list{1,2})
        case 'cell'
            s{k} = parameter_list{1,2}{k};
        case {'double','logical'}
            s{k} = num2str(parameter_list{1,2}(k));
        otherwise
            s{k} = parameter_list{1,2}(k);
    end
    disp([s{k} ...
        ' Mean ' num2str(mean(x(k).snrvector)) ...
        ' Std ' num2str(std(x(k).snrvector))])
end
set(gca,'FontName','Times','Fontsize',18)
legend(s,'location','northwest')
xlabel('pSNR')
ylabel('Occurences')


if this_argnum==2
    figure(2)
    clf
    [h,hix] = hist(x(1).snrvector-x(2).snrvector,100);
    plot(hix,h,'linewidth',2)
    set(gca,'FontName','Times','Fontsize',18)
    legend([s{1} ' - ' s{2}],'location','northwest')
    xlabel('\Delta pSNR')
    ylabel('Occurences')
    grid on
    
    disp([s{1} ' - ' s{2} ...
        ' mean ' num2str(mean(x(1).snrvector-x(2).snrvector)) ...
        ' std ' num2str(std(x(1).snrvector-x(2).snrvector))])
    
    addpath swtest/
    ix = randperm(length(x(1).snrvector),5000);
    isnormal = swtest(x(1).snrvector(ix)-x(2).snrvector(ix));
    disp(['Difference is normal (Saphiro-Wilks on subset of 5000): ' num2str(isnormal)])
    issignificant = ttest(x(1).snrvector - x(2).snrvector);
    disp(['Difference is significant (t-test): ' num2str(issignificant)])
    
else
    
    figure(2)
    clf
    X = zeros(this_argnum,length(x(1).snrvector));
    for k=1:this_argnum, 
        X(k,:) = x(k).snrvector; 
        
    end
    boxplot(X',s,'notch','on')
    grid on
    set(gca,'FontName','Times','Fontsize',18)
    l = parameter_list{1,1};
    l(l == '_') = ' ';
    xlabel(l)
    ylabel('pSNR (dB)')
end