function phon = readphn(filename)

f = fopen(filename);
ix = 0;
while 1
    tline = fgetl(f);
    if ~ischar(tline), break, end
    ix = ix+1;
    d = find(tline == ' ');
    phon(ix).start = str2num(tline(1:d(1)-1));
    phon(ix).end = str2num(tline((d(1)+1):(d(2)-1)));
    phon(ix).name = tline((d(2)+1):end);
end


fclose(f);