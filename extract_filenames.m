function [d_train,d_test,filenum_train,filenum_test] = extract_filenames(timit_path,temp_path)


try 
    timit_path_old = timit_path; clear timit_path
    load([temp_path 'filelist.mat'])
    if ~exist('timit_path') || ~strcmp(timit_path,timit_path_old)
        timit_path = timit_path_old;
        error(' ')
    end
catch
    disp('Collecting file names')
    
    if ~exist('timit_path'), timit_path = timit_path_old; clear timit_path_old; end
        d_train = dirr([timit_path 'TRAIN/'],'.WAV');
        d_test = dirr([timit_path 'TEST/'],'.WAV');
        if length(d_train)==0
            d_train = dirr([timit_path 'train/'],'.wav');
            d_test = dirr([timit_path 'test/'],'.wav');
        end
        save([temp_path 'filelist.mat'], 'd_train', 'd_test','timit_path')
end
filenum_train = length(d_train);
filenum_test = length(d_test);

