function [x,ix] = get_mode(source_list, target)


for ix=1:length(source_list) 
    if check_mode(source_list(ix).codec_mode, target)
        x = source_list(ix);
        return
    end
end
x = [];
ix = -1;