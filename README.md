# DITHERED SPEECH AND AUDIO CODEC

NOTE: This project has been documented here for historical reference and is not
currently in active development. Instead, we are working a python/pytorch-version 
of the same codec which allows end-to-end training. The new codec is intended to 
be published at 
https://gitlab.com/groups/speech-interaction-technology-aalto-university/

## Quick start
* Run *test_dithered_codec.m* in Matlab to initialize all.
* Codec parameters, such as bitrate and window length, are specified
in *generate_parameters.m*
* Initialization will create a database of .mat-files in */l/temp/*. This path can
be changed in *generate_parameters.m*
* Assumes that the TIMIT corpus is available at the path defined
in *generate_parameters.m*

## Overview

This is a platform for testing speech and audio coding in academic research.
The main objective is to study statistical methods for speech and audio coding,
for improving overall performance and enabling distributed coding.

There is reason to believe that some technologies in this software package
patented and therefore this platform cannot be used for anything else than
academic research. Commercial use is quite explicitly forbidden.

The codec is loosely derived from the TCX mode in the 3GPP EVS standard. In
other words, it is frequency-domain codec based on MDCT. The spectral envelope
is modelled in the cepstral domain. The perceptual model is currently a
placeholder until we develope something better; specifically, we have two
alternatives, a perceptual model derived from EVS and a heuristic hand-waving
approach. The former is computationally inefficient and optimized for a sampling
rate of 12.8kHz, while the latter is a quick mock-up of what a proper model
would do.

The envelope parameters (20 cepstral coefficients) are quantized with such
an accuray that we obtain a mean distortion of 1 dB in the resynthsised envelope.
The parameters are further encoded with a variable-bitrate arihmetic coder,
which assumes that all parameters are Gaussian. The parameters are estimated
over the TIMIT training set.
Using the perceptual model and the envelope, we can then deduce an expectation
for the bitrate of each spectral component.

The spectral components are encoded with a dithered, hybrid entropy coder. All
components where the bitrate is larger than 1bit/sample, are encoded with an
arithmetic coder, where the quantization step size is chosen to match the desired
bitrate. The quantization bins are further randomly shifted to get the effect
of dithering. Components with 0 to 1 bits are quantized with a randomized
quantizer as follows. We generate a random rotation into a subspace where we
quantize the sign of the *K* first components (scaled for maximum SNR) and then
apply the inverse rotation. Components where the bitrate is below zero, are replaced
by random noise such that the desired amount of energy is retained.


## Technical description

## Systems structure

The codec is a frequency domain codec based on the MDCT transform. The spectral envelope shape is modeled with a cepstral model. The perceptual model is derived from the quantized envelope model. The cepstral coefficients are quantized with a uniform quantizer and encoded with a variable rate entropy coder. The quantized envelope model is used to flatten the spectrum such that it is an approximately unit-variance and zero mean. The relative bitrate of each spectral component is obtained from the log2-ratio of the perceptual and envelope models; it corresponds to the perceptual entropy.
The absolute bitrate is then obtained from the relative bitrate by shifting and truncating it at zero, in an iterative process. The spectral components are then quantized and encoded with a hybrid of two entropy coders; components with an expected bitrate above 1bit/sample with uniform, dithered quantization and arithmetic coding, and those with a bitrate between 0 and 1 bit/sample, with a randomized 1bit quantizer. 


