function muh = expectation_truncated_laplacian(L,R,mu,s)

absL = sqrt(2)*abs((L-mu)./s);
absR = sqrt(2)*abs((R-mu)./s);
expL = exp(-absL);
expR = exp(-absR);
xfxL = (1/sqrt(8))*(1 - expL.*(absL + 1));
xfxR = (1/sqrt(8))*(1 - expR.*(absR + 1));
cL = 0.5*sign(L).*(1-expL);
cR = 0.5*sign(R).*(1-expR);

muh = (xfxR - xfxL)./(eps + cR - cL);


% sanity
muh = max(muh,L);
muh = min(muh,R);
