clear
if length(dbstack) == 1
    disp('Train envelope coder for dithered coding experiments')
end
disp('  Log-postfilter')


[codec_mode,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters();
[d_train,d_test,filenum_train,filenum_test] = extract_filenames(timit_path,temp_path);


try
    load(envstat_filename)
    EnvStat = get_mode(EnvStat,codec_mode);
    if ~isstruct(EnvStat), error(' '), end
catch
    disp('Training data not found. Generating training data.')
    
    train_all
    
    [codec_mode,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters();
    [d_train,d_test,filenum_train,filenum_test] = extract_filenames(timit_path,temp_path);


    load(envstat_filename)
    EnvStat = get_mode(EnvStat,codec_mode);
    
end

% Envelope pdf
if ~codec_mode.envelope_covariance_diagonal
    EnvStat.pdf = EnvStat.GMModel{codec_mode.envelope_mixture_order};
    for k=1:codec_mode.envelope_mixture_order
        EnvStat.iCov(:,:,k) = pinv(EnvStat.pdf.Sigma(:,:,k));
        for h=1:codec_mode.envelope_order
            EnvStat.CovDet(k,h) = det(EnvStat.pdf.Sigma(1:h,1:h,k));
        end
    end
end

EnvFn = codec_mode.EnvFn;
iLogEnvFn = codec_mode.iLogEnvFn;

load([temp_path 'Xmat.mat'])

Xmat = Xmat(:,1:(10^5));
wincnt = size(Xmat,2);

bits_env = zeros(size(Xmat,2),1);
%yq = zeros(size(Xmat));
waith = waitbar(0,'Encoding');
for k=1:size(Xmat,2)
    if mod(k,100)==0, waitbar(k/size(Xmat,2),waith), end
    Env = EnvFn(Xmat(:,k));

    Ql = floor(Env/EnvStat.dq_env)*EnvStat.dq_env;
    Qu = ceil(Env/EnvStat.dq_env)*EnvStat.dq_env;
        
    [bits_env(k),Envq] = envelope_coder(Ql,Qu,EnvStat,codec_mode,bits_per_frame);
    %yq(:,k) = iLogEnvFn(Envq);
    
    % Reconstructed envelop (std of signal)
    Xstd = diag(EnvStat.specstd)*(10.^((iLogEnvFn(Envq))/20));   
    
    Xmat(:,k) = Xmat(:,k)./Xstd;
end
close(waith)


F0_range = [70 450];
T0_range = round(fliplr(codec_mode.sampling_frequency./F0_range));
T0count = (T0_range(2)-T0_range(1)+1);

T0 = zeros(wincnt,1);
for k=1:wincnt %randperm(wincnt)
    d = dct(abs(Xmat(:,k)));
    [~,T0h] = max(d(1+(T0_range(1):T0_range(2))));
    T0(k) = T0h+T0_range(1);
    
    if 0
        clf
        plot((1:speclen),d)
        hold on
        plot(T0(k)*[1 1],[0 d(T0(k))],'x-r')
        waitforbuttonpress
    end
end

F0postfilter = zeros(speclen,T0count);
for k=1:T0count
    T0k = T0_range(1)+k-1;
    tix = find(T0 == T0k);
    if numel(tix) == 0, 
        F0postfilter(:,k) = 1;
        continue
    end
    
    F0postfilter(:,k) = std(Xmat(:,tix),0,2);
    
    Xmat(:,tix) = diag(1./F0postfilter(:,k))*Xmat(:,tix);
    
    if 0
        clf
        plot(-log2(F0postfilter(:,k)))
        title(num2str(50*[-log2(T0count)+sum(-log2(F0postfilter(:,k)))]))
        waitforbuttonpress
    end
    
    if 0
        clf
        hix = linspace(-10,10,100);
        H = zeros(speclen,numel(hix));
        for h = 1:speclen
            H(h,:) = hist(Xmat(h,tix)/F0postfilter(h,k),hix);
            F(h,:) = hist(Xmat(h,tix),hix);
        end
        imagesc([F; H])
        waitforbuttonpress
    end
end

if 1
    clf
    imagesc(F0postfilter)
    drawnow
end

EnvStat.F0postfilter = F0postfilter;
EnvStat.F0postfilter_T0range = T0_range;


EnvStat_this = EnvStat;
load(envstat_filename)
[~,ix] = get_mode(EnvStat,codec_mode,'envelope_order','envelope_mode',...
                    'envelope_quantization_target_dB','envelope_expectation_decoder');
f = fieldnames(EnvStat_this);
for k=1:length(f)    
    %eval(['EnvStat(ix).' f{k} ' = getfield(EnvStat_this,f{k});']);
    EnvStat(ix).(f{k}) = getfield(EnvStat_this,f{k});
end
save(envstat_filename,'EnvStat');





%%
disp('  Spectral component distributions after postfiltering')



clf
options = statset('MaxIter',1000);
sign_gain = zeros(speclen,1);
spectral_pdf = struct();
for k=1:speclen %randperm(speclen)
    %sign_gain(k,1) = gainfn(sign(Xmat(k,:)),Xmat(k,:));
    %disp(num2str(sign_gain(k,1)))
    
    [h,hix] = hist(Xmat(k,:),200);
    
    %lodfpdf(k) = fit_logistic_mixture(Xmat(k,:)',2:5);
    neglog = zeros(5,1)+NaN; aic = neglog; b = neglog;
    for mc=1:3
        GMModel = fitgmdist(Xmat(k,:)',mc,'options',options);
        neglog(mc) = GMModel.NegativeLogLikelihood;
        aic(mc) = GMModel.AIC;
        
        s = zeros(mc,1); mu = s; proportion = s;
        f = [];
        for comp=1:mc
            mu(comp) = GMModel.mu(comp);        
            proportion(comp) = GMModel.ComponentProportion(comp);
            %s= sqrt(v*3)/pi
            s(comp,1) = sqrt(3*GMModel.Sigma(:,:,comp))/pi; 
            f(comp,:) = 1./(1 + exp(-(hix-mu(comp))/s(comp)));
            f(comp,:) = (proportion(comp)./s(comp))*(f(comp,:).*(1-f(comp,:)));    
        end
        f(mc+1,:) = sum(f(1:comp,:),1);
        b(mc,1) = -sum(f(end,:).*log2(eps+f(end,:)));
        
        spectral_pdf(k,mc).mu = mu;
        spectral_pdf(k,mc).s = s;
        spectral_pdf(k,mc).proportion = proportion;
        spectral_pdf(k,mc).neglog = GMModel.NegativeLogLikelihood;
        spectral_pdf(k,mc).aic = GMModel.AIC;

        if mc > 1
            clf            
            subplot(221)            
            plot(hix,h/((hix(2)-hix(1))*sum(h)),'LineWidth',2)
            hold on
            plot(hix, f)
            axis tight
            title('Training spectral component distributions')

            subplot(222)
            plot(aic)        
            title(['AIC at ' num2str([k mc])])
            subplot(223)
            plot(neglog)        
            title(['NegLogLike at ' num2str([k mc])])
            subplot(224)
            %plot([neglog/(size(Xmat,2)*log(2)) b+log2(hix(2)-hix(1))])
            plot(b)
            title('bits')
            drawnow
        end
    end
end

%EnvStat(ix).sign_gain = sign_gain;
EnvStat(ix).spectral_postF0_pdf = spectral_pdf;
save(envstat_filename,'EnvStat')

