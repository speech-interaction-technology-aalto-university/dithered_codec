function train_envelope_model(codec_mode,force)

if length(dbstack) == 1
    disp('Train envelope coder for dithered coding experiments')
    codec_mode = generate_parameters();
end

if nargin < 2
    force = 0;
end

bitrate = 10^6; % arbitrarily large

[~,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters();


EnvStat = load_statistics(codec_mode,envstat_filename,true);
if ~force
    if isfield(EnvStat,'dq_env') && length(EnvStat.dq_env)
        return % already trained
    else 
        % try to find an equivalent model to avoid retraining for speedup
        EnvStat_match = load_matching_statistics(codec_mode,envstat_filename, {...    
            'sampling_frequency',...
            'envelope_mode',....
            'envelope_order',...
            'envelope_exponent',...
            'envelope_quantization_target_dB'},...
            {'dq_env'});
        if isstruct(EnvStat_match) && length(EnvStat.dq_env)
            EnvStat.dq_env = EnvStat_match.dq_env;
            save_statistics(codec_mode,envstat_filename,EnvStat);

            return
        end    
    end
end

disp('  Envelope quantization')
load([temp_path 'Xmat.mat'])


EnvFn = codec_mode.EnvFn;
iLogEnvFn = codec_mode.iLogEnvFn;
speclen = codec_mode.speclen;

Xmat = Xmat(:,randperm(size(Xmat,2),5000));
wincnt = size(Xmat,2);

%EnvFn = EnvStat.EnvFn;
%iLogEnvFn = EnvStat.iLogEnvFn;
%Env = EnvMtx*(20*log10(abs(Xmat)));
Env = EnvFn(Xmat);

clear Xmat;

%y = iEnvMtx*Env;
y = iLogEnvFn(Env);

dq_list = [1 2]*sqrt(12)*sqrt(speclen/(codec_mode.envelope_order+1));
%yq = iLogEnvFn(dq_list(1)*round(Env/dq_list(1)));
Ql = floor(Env/dq_list(1))*dq_list(1);
Qu = ceil(Env/dq_list(1))*dq_list(1);
        
[~,Envq] = envelope_coder(Ql,Qu,EnvStat,codec_mode,bitrate);
yq = iLogEnvFn(Envq);

%e = [sqrt(mean((y(:)-yq(:)).^2)) Inf];
e = [std(y(:)-yq(:)) Inf];

clf 
plot(dq_list(1),e(1),'x')
hold on
title('Distortion of envelope quantizer (dashed is target)')
plot(min(dq_list)*[.2 2],codec_mode.envelope_quantization_target_dB*[1 1],'k--')
drawnow


while e(1) > codec_mode.envelope_quantization_target_dB
    dq_list = dq_list/2;
    %yq = iLogEnvFn(dq_list(1)*round(Env/dq_list(1)));
    Ql = floor(Env/dq_list(1))*dq_list(1);
    Qu = ceil(Env/dq_list(1))*dq_list(1);

    [~,Envq] = envelope_coder(Ql,Qu,EnvStat,codec_mode,bitrate);
    yq = iLogEnvFn(Envq);
    e(2) = e(1);
    e(1) = std(y(:)-yq(:));
    plot(dq_list(1),e(1),'rx')
    drawnow
end

if isfinite(e(2)) == 0
%yq = iLogEnvFn(dq_list(2)*round(Env/dq_list(2)));
    Ql = floor(Env/dq_list(2))*dq_list(2);
    Qu = ceil(Env/dq_list(2))*dq_list(2);

    [~,Envq] = envelope_coder(Ql,Qu,EnvStat,codec_mode,bitrate);
    yq = iLogEnvFn(Envq);
    e(2) = sqrt(mean((y(:)-yq(:)).^2));
end
while e(2) < codec_mode.envelope_quantization_target_dB
    dq_list(2) = dq_list(2)*2;
    %yq = iLogEnvFn(dq_list(2)*round(Env/dq_list(2)));
    Ql = floor(Env/dq_list(2))*dq_list(2);
    Qu = ceil(Env/dq_list(2))*dq_list(2);

    [~,Envq] = envelope_coder(Ql,Qu,EnvStat,codec_mode,bitrate);
    yq = iLogEnvFn(Envq);
    e(2) = std(y(:)-yq(:));
    plot(dq_list(2),e(2),'o')
    drawnow
end

for k=1:8
    dqmid = mean(dq_list);
    %yq = iLogEnvFn(dqmid*round(Env/dqmid));
    Ql = floor(Env/dqmid)*dqmid;
    Qu = ceil(Env/dqmid)*dqmid;

    [~,Envq] = envelope_coder(Ql,Qu,EnvStat,codec_mode,bitrate);
    yq = iLogEnvFn(Envq);

    emid = std(y(:)-yq(:));
    plot(dqmid,emid,'+')
    drawnow

    if emid > codec_mode.envelope_quantization_target_dB
        e(2) = emid;
        dq_list(2) = dqmid;
    else
        e(1) = emid;
        dq_list(1) = dqmid;
    end    
    
end
dq_env = dq_list(1);
EnvStat.dq_env = dq_env;


save_statistics(codec_mode,envstat_filename,EnvStat);
