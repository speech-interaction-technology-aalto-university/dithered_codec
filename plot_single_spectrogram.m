function plot_single_spectrogram(x,codec_mode)

winnum = floor((length(x)-codec_mode.window_length)/codec_mode.window_step)+1;

Xmat = zeros(codec_mode.speclen,winnum);
for k=1:winnum
    ix = (k-1)*codec_mode.window_step + (1:codec_mode.window_length);
    xwin = x(ix).*codec_mode.hwin;

    X = codec_mode.MdctMtx*xwin;
    Xmat(:,k) = X;
end

imagesc(20*log10(10^-7+abs(Xmat)))
set(gca,'YDir', 'normal');


