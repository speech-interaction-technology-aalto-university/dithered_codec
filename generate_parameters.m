function [codec_mode,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters()

%% Off-line training configuration
codec_mode = struct();
codec_mode.sampling_frequency = 16000;
codec_mode.noise_fill = true; % for bins with 0 bits for encoding
codec_mode.quantizer_1bit = true; % for bins with 0 to 1 bits
codec_mode.quantizer_1bit_mode = 'noiseshaping';
%codec_mode.quantizer_1bit_mode = 'randomrotation';
codec_mode.dithering = true; % for non-1bit quantizer
codec_mode.truncate_zeros = true; % when dithering is off
codec_mode.decoder_mode = 'mean'; % spectrum decoder 'mid','mean' or 'var'
codec_mode.envelope_mode = 'triangular_mel'; % cepstrum or mfcc or mfcc2
codec_mode.envelope_order = 12;
codec_mode.envelope_exponent = 2;
codec_mode.envelope_quantization_target_dB = 1.9;
codec_mode.envelope_covariance_diagonal = false; % restrict to diagonal covariance (true/false), false is better&slower
codec_mode.envelope_mixture_order = 5;
codec_mode.envelope_expectation_decoder = true; % calculate expectations in envelope quantization bins in decoder (slower) - not recommended
%codec_mode.spectrum_distribution = 'laplacian';
codec_mode.spectrum_distribution = 'logistic mixture';
%codec_mode.encoder_F0_model = true;
%codec_mode.postfilter_context_size = 10;
%codec_mode.postfilter_maxlag = 4;
codec_mode.perceptual_model_mode = 'homebrew';
%codec_mode.perceptual_model_mode = 'ACELP';




%% Paths
timit_path = '/l/sounds/TIMITCD/timit/';
temp_path = '/l/temp/dithered_codec_db/';

if numel(dir(temp_path)) == 0
    eval(['!mkdir ' temp_path])
end

%% Windowing
codec_mode.window_length = 30*codec_mode.sampling_frequency/1000;
codec_mode.window_step = 20*codec_mode.sampling_frequency/1000;
window_overlap = codec_mode.window_length-codec_mode.window_step;
window_flat = codec_mode.window_length - 2*window_overlap;

%frames_per_second = codec_mode.sampling_frequency/codec_mode.window_step;

codec_mode.windowtype = 'halfsine';

switch lower(codec_mode.windowtype)
    case {'halfsine'}
        hwin_ramp = sin(pi*(.5:window_overlap)/(2*window_overlap))';
        codec_mode.hwin = [hwin_ramp; ones(window_flat,1); flipud(hwin_ramp)];
    case {'dpss_ola'}
        codec_mode.hwin = dpss_ola(9.5,window_length,window_flat);
    case {'kbd'}        
        hwin = kbd(window_overlap*2,5);
        codec_mode.hwin = [hwin(1:window_overlap); ones(window_flat,1); flipud(hwin(1:window_overlap))];
end

MdctMtx = mdct_matrix(codec_mode.window_step*2);
discard_len = (codec_mode.window_step*2 - codec_mode.window_length)/2;        
codec_mode.MdctMtx = MdctMtx(:,discard_len+(1:codec_mode.window_length));

%if strcmp(codec_mode.perceptual_model_mode,'ACELP')
%    codec_mode.MdctMtx = MdctMtx;
%    codec_mode.hwin = hwin;
%end

codec_mode.speclen = size(MdctMtx,1);

gainfn = @(x,ref) (x(:)'*ref(:))/(x(:)'*x(:));
normedfn = @(x,ref) x*gainfn(x,ref);


%% Envelope
envstat_filename = [temp_path 'EnvStat.mat'];
codec_mode = generate_envelope_model(codec_mode);


%% Default runtime mode
runtime_mode = runtime_configuration(codec_mode);



%% Print configuration
if length(dbstack) < 3 % print configuration only when called from the highest-parent function

    disp('    Off-line trained configuration')
    %s = ['       Bitrate: ' num2str(bitrate_per_s/1000) ' kbps'];
    %disp(s)
    f = fieldnames(codec_mode);
    for k=1:length(f)    
        s = ['       ' f{k} ': '];
        switch class(codec_mode.(f{k}))
            case 'double'
                if numel(codec_mode.(f{k})) == 1
                    s = [s num2str(codec_mode.(f{k}))];
                else
                    continue
                end
            case 'logical'
                if codec_mode.(f{k}), s = [s 'true'];
                else s= [s 'false']; end
            case 'char'
                s = [s codec_mode.(f{k})];
            otherwise
                continue
        end
        disp(s)
    end
    
    disp('    Run-time mode')
    f = fieldnames(runtime_mode);
    for k=1:length(f)    
        s = ['       ' f{k} ': '];
        switch class(runtime_mode.(f{k}))
            case 'double'
                if numel(runtime_mode.(f{k})) == 1
                    s = [s num2str(runtime_mode.(f{k}))];
                end
            case 'logical'
                if runtime_mode.(f{k}), s = [s 'true'];
                else s= [s 'false']; end
            case 'char'
                s = [s runtime_mode.(f{k})];
            otherwise
                continue
        end
        disp(s)
    end
    
end