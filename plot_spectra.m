function plot_spectra(parameter_list)


parameter_count = size(parameter_list,1);

% default configuration
[codec_mode,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters();
[d_train,d_test,filenum_train,filenum_test] = extract_filenames(timit_path,temp_path);

x=readsph(d_test{1},[],-1,-1);

s = d_test{1};
ix = find(s == '/'); 
s(ix) = [];
s = [temp_path s];
audiowrite(s,x*0.9/max(abs(x)),codec_mode.sampling_frequency)

for ix=1:parameter_count
    if strcmp(class(parameter_list{ix,2}),'char')
        this_argnum = 1;
    else
        this_argnum = length(parameter_list{ix,2});
    end
   if this_argnum == 1
       codec_mode.(parameter_list{ix,1}) = parameter_list{ix,2};
       
       switch parameter_list{ix,1}
           case {'envelope_order', 'envelope_mode', 'envelope_covariance_diagonal'}
               codec_mode = generate_envelope_model(codec_mode);
       end
       
       s = [s '_' parameter_list{ix,1} '_'];
           switch class(parameter_list{ix,2})
               case {'double','logical'}
                   s = [s num2str(parameter_list{ix,2})];
               case 'char'
                   s = [s parameter_list{ix,2}];
           end       
       
   else
       
       
       clf
       subplot(this_argnum+1,1,1)
       plot_single_spectrogram(x,codec_mode);
       
       
       for k=1:this_argnum
           parameter_list_excerpt = parameter_list;
           switch class(parameter_list{ix,2})
               case {'double','logical'}
                   parameter_list_excerpt{ix,2} = parameter_list{ix,2}(k);
               case 'cell'
                   parameter_list_excerpt{ix,2} = parameter_list{ix,2}{k};
           end
           
           subplot(this_argnum+1,1,k+1)
           plot_spectra(parameter_list_excerpt);
       end
       return
   end
end


EnvStat = load_statistics(codec_mode,envstat_filename);

xq = dithered_codec(x,codec_mode,runtime_mode,EnvStat);
plot_single_spectrogram(xq,codec_mode);

s = [s '.wav'];
audiowrite(s,xq*0.9/max(abs(xq)),codec_mode.sampling_frequency)

return













