clear


pdf = struct('mu',0,'s',1,'proportion',1);


N = 1000000;
L = 0.8729;
R = 2.7211;

x = -log(1./rand(N,1) - 1);
x(x < L) = [];
x(x > R) = [];

mu = mean(x);
muh = expectation_truncated_logistic_mixture(L,R,pdf);

disp(num2str([mu muh]))