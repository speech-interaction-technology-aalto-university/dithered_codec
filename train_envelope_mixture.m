function train_envelope_mixture(codec_mode,force)

disp('Train envelope coder for dithered coding experiments')

if nargin < 2
    force = 0;
end

[~,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters();
%[d_train,d_test,filenum_train,filenum_test] = extract_filenames(timit_path,temp_path);


EnvStat = load_statistics(codec_mode,envstat_filename,true);
if ~force
    if isfield(EnvStat,'GMModel') && length(EnvStat.GMModel)
        return % already trained
    else 
        % try to find an equivalent model to avoid retraining for speedup
        EnvStat_match = load_matching_statistics(codec_mode,envstat_filename, {...    
            'sampling_frequency',...
            'envelope_mode',....
            'envelope_exponent',...
            'envelope_order'},...
            {'EnvMu','EnvStd','GMModel'});
        if isstruct(EnvStat_match)
            EnvStat.codec_mode = codec_mode;
            EnvStat.EnvMu = EnvStat_match.EnvMu;
            EnvStat.EnvStd = EnvStat_match.EnvStd;
            EnvStat.GMModel = EnvStat_match.GMModel;
            save_statistics(codec_mode,envstat_filename,EnvStat);

            return
        end

    end
end

disp('  Envelope mixture models')

load([temp_path 'Xmat.mat'])

wincnt = size(Xmat,2);
EnvStat.codec_mode = codec_mode;
EnvFn = codec_mode.EnvFn;
iLogEnvFn = codec_mode.iLogEnvFn;
Env = EnvFn(Xmat);

clear Xmat;

EnvMu = mean(Env,2);
EnvStd = std(Env,0,2);

EnvStat.EnvMu = EnvMu;
EnvStat.EnvStd = EnvStd;

mixture_order_list = 1:5;

GMModel = cell(0);
options = statset('MaxIter',1000);
for mix = 1:numel(mixture_order_list)

    mixture_order = mixture_order_list(mix);
    
    GMModel{mix} = fitgmdist(Env',mixture_order,'Options',options);
    
    AIC(mix) = GMModel{mix}.AIC;
    negLogLike(mix) = GMModel{mix}.NegativeLogLikelihood;
    
    % visualization
    clf
    subplot(211)
    plot(AIC/wincnt)    
    title('AIC of envelope model')
    grid on
    subplot(212)
    plot(log2(exp(1))*negLogLike/wincnt)
    title('Relative bitrate of envelope model')
    grid on
    drawnow
end


EnvStat.GMModel = GMModel;
save_statistics(codec_mode,envstat_filename,EnvStat);

