function s = generate_evaluation_file_name(temp_path,parameter_list)

parameter_count = size(parameter_list,1);

s = [temp_path 'evaluation-'];
for k=1:parameter_count
    s = [s parameter_list{k,1} '-'];
    switch class(parameter_list{k,2})
        case 'char'
            s = [s parameter_list{k,2}];
        case {'double', 'logical'}
            s = [s num2str(parameter_list{k,2})];
    end
    if k < parameter_count, s=[s '-']; end
end
s = [s '.mat'];