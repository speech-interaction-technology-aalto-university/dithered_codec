clear

fb_list = {'cepstrum'; 'sfb_uniform'; 'sfb_mel'; 'triangular_uniform'; 'triangular_mel'};


codec_mode = generate_parameters();

for k=1:length(fb_list)
   
   codec_mode.envelope_mode = fb_list{k};
   [codec_mode,EnvMtx] = generate_envelope_model(codec_mode);
   
   
   clf
   plot(EnvMtx')
   title(fb_list{k})
    
   waitforbuttonpress 
end