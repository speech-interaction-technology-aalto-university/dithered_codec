clear

disp('Running all entropy evaluations')


plot_results({'envelope_mode',{'mfcc', 'cepstrum'}});
plot_results({'envelope_order',8:2:22});
plot_results({'envelope_covariance_diagonal',[true false]});
plot_results({'envelope_mixture_order',1:5});
plot_results({'envelope_quantization_target_dB',[.8:.2:1.6 1.7:.1:2.0 2.2]});
plot_results({'spectrum_distribution',{'laplacian','logistic mixture'}});
plot_results({'dithering',[true false]});
plot_results({'quantizer_1bit',[true false]});
plot_results({'quantizer_1bit_mode',{'randomrotation','noiseshaping'}});
