function [bits_env,Envq] = envelope_coder(Ql,Qu,EnvStat,codec_mode,bits_per_frame)

if size(Ql,2) > 1
    bits_env = zeros(size(Ql,2),1);
    Envq = zeros(size(Ql));
    for k=1:size(Ql,2)
        [bits_env(k),Envq(:,k)] = envelope_coder(Ql(:,k),Qu(:,k),EnvStat,codec_mode,bits_per_frame);
    end
    return
end

        if codec_mode.envelope_expectation_decoder
            Envq = expectation_truncated_normal(Ql,Qu,EnvStat.EnvMu,EnvStat.EnvStd);
        else
            Envq = 0.5*(Ql+Qu);            
        end

        if codec_mode.envelope_covariance_diagonal
            
            
            Cl = fast_normcdf(Ql,EnvStat.EnvMu,EnvStat.EnvStd);
            Cu = fast_normcdf(Qu,EnvStat.EnvMu,EnvStat.EnvStd);
            
            % Bit consumption of envelope
            bvec = -log2(Cu-Cl);
            cvec = cumsum(bvec);
            qq = codec_mode.envelope_order;
            while cvec(qq) > bits_per_frame         % overflow check    
                cvec(qq) = 0;
                qq = qq-1;
            end                
            Envq((qq+1):end) = EnvStat.EnvMu((qq+1):end);        
            
            bits_env = cvec(qq);
            
        else
            % This approach is from paper: 
            %
            % Korse, Srikanth, Guillaume Fuchs, and Tom Bäckström. 
            % "GMM-based iterative entropy coding for spectral envelopes 
            % of speech and audio." 2018 IEEE International Conference on 
            % Acoustics, Speech and Signal Processing (ICASSP). IEEE, 2018.
            
            Cl = zeros(size(Ql));
            Cu = zeros(size(Ql));
            bvec = zeros(size(Ql));
            cvec = bvec;
            
            
            EnvStatpdfmu = EnvStat.pdf.mu; % set up local variable for faster access
            if codec_mode.envelope_mixture_order == 1

                overflow = 0;
                
                Cl(1) = fast_normcdf(Ql(1),EnvStatpdfmu(1),sqrt(EnvStat.pdf.Sigma(1,1)));
                Cu(1) = fast_normcdf(Qu(1),EnvStatpdfmu(1),sqrt(EnvStat.pdf.Sigma(1,1)));                
                
                bvec(1) = -log2(Cu(1)-Cl(1));
                cvec(1) = bvec(1);
                if cvec(1) > bits_per_frame
                    cvec(:) = 0;
                    Envq = EnvStatpdfmu;
                else

                    for eix = 2:codec_mode.envelope_order
                        foo = (Envq(1:eix-1) - EnvStatpdfmu(1:eix-1)');
                        A1 = EnvStat.iCov(eix,eix);
                        iA1 = 1./A1;
                        A01 = EnvStat.iCov(eix,1:eix-1)';
                        muh = EnvStatpdfmu(eix) - iA1*(A01'*foo);
                        %c  = foo'*( A01*A01'*iA1 - A1)*foo;

                        Cl(eix) = fast_normcdf(Ql(eix),muh,sqrt(iA1));
                        Cu(eix) = fast_normcdf(Qu(eix),muh,sqrt(iA1));
                        bvec(eix) = -log2(Cu(eix)-Cl(eix));
                        cvec(eix) = cvec(eix-1) + bvec(eix);

                        if cvec(eix) > bits_per_frame % overflow check
                            cvec(eix:end) = bits_per_frame;
                            A01 = EnvStat.iCov(1:eix-1,eix:end);
                            iA1 = pinv(EnvStat.iCov(eix:end,eix:end));
                            Envq(eix:end)= EnvStatpdfmu(eix:end)' - iA1*A01'*foo;
                            break
                        end
                    end
                end
                
                bits_env = cvec(end);
                
                
            else
                clmix = zeros(codec_mode.envelope_mixture_order,1);
                cumix = clmix;               
                EnvStatpdfSigma = EnvStat.pdf.Sigma;
                EnvStatpdfComponentProportion = EnvStat.pdf.ComponentProportion;
                EnvStatCovDet = EnvStat.CovDet;
                EnvStatiCov = EnvStat.iCov;
                for mix=1:codec_mode.envelope_mixture_order
                    clmix(mix) = fast_normcdf(Ql(1),EnvStatpdfmu(mix,1),sqrt(EnvStatpdfSigma(1,1,mix)));
                    cumix(mix) = fast_normcdf(Qu(1),EnvStatpdfmu(mix,1),sqrt(EnvStatpdfSigma(1,1,mix)));                    
                end
                Cl(1) = EnvStatpdfComponentProportion*clmix;
                Cu(1) = EnvStatpdfComponentProportion*cumix;
                
                bvec(1) = -log2(Cu(1)-Cl(1));
                cvec(1) = bvec(1);
                                                
                for eix = 2:codec_mode.envelope_order
                    ComponentProportion = zeros(1,codec_mode.envelope_mixture_order);
                    for mix=1:codec_mode.envelope_mixture_order
                        foo = (Envq(1:eix-1) - EnvStatpdfmu(mix,1:eix-1)');
                        A0 = EnvStatiCov(1:eix-1,1:eix-1,mix);
                        A1 = EnvStatiCov(eix,eix,mix);
                        iA1 = 1./(eps+A1);
                        A01 = EnvStatiCov(eix,1:eix-1,mix)';
                        muh = EnvStatpdfmu(mix,eix) - iA1*(A01'*foo);
                        c  = foo'*( A01*A01'*iA1 - A0)*foo;                        
                                                
                        ComponentProportion(mix) = EnvStatpdfComponentProportion(mix) ...
                            * exp(c) * sqrt(A1 / (eps+EnvStatCovDet(mix,eix))) ...
                            + eps;
                        clmix(mix) = fast_normcdf(Ql(eix),muh,sqrt(iA1));
                        cumix(mix) = fast_normcdf(Qu(eix),muh,sqrt(iA1));
                    end
                    ComponentProportion = ComponentProportion/sum(ComponentProportion);
                    Cl(eix) = ComponentProportion*clmix;
                    Cu(eix) = ComponentProportion*cumix;
                    
                    bvec(eix) = -log2(eps+Cu(eix)-Cl(eix));
                    cvec(eix) = cvec(eix-1) + bvec(eix);
                    
                    if cvec(eix) > bits_per_frame
                        cvec(eix:end) = bits_per_frame;                        
                        pred_num = codec_mode.envelope_order-eix+1;
                        muh = zeros(pred_num,codec_mode.envelope_mixture_order);
                        for mix=1:codec_mode.envelope_mixture_order
                            A01 = EnvStat.iCov(1:eix-1,eix:end,mix);
                            iA1 = pinv(EnvStat.iCov(eix:end,eix:end,mix));
                        	muh(1:pred_num,mix) = EnvStatpdfmu(mix,eix:end)' - iA1*A01'*foo;
                        end
                        Envq(eix:end) = muh*ComponentProportion';
                        
                        break
                    end
                end
                
                bits_env = cvec(end);
                
                
            end
            
        end               
