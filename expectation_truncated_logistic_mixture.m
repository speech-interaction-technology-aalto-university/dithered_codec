function muh = expectation_truncated_laplacian(L,R,pdf)

muh = zeros(size(L));
for k = 1:length(L)
    Lm = (L(k) - pdf(k).mu)./pdf(k).s;
    Rm = (R(k) - pdf(k).mu)./pdf(k).s;

    C_Lm = 1./(1 + exp(-Lm));
    C_Rm = 1./(1 + exp(-Rm));

    Ixfx_Lm = Lm.*C_Lm - max(Lm,log(1+ exp(min(35,Lm)))); % above 35, log(1+e^x)=x, but for very large x, exp goes to infinity
    Ixfx_Rm = Rm.*C_Rm - max(Rm,log(1+ exp(min(35,Rm))));

    CompP = C_Rm - C_Lm;
    CompMu = pdf(k).mu + pdf(k).s.*(Ixfx_Rm - Ixfx_Lm)./(eps + CompP);
    ix = find((CompMu < L(k)) | (CompMu > R(k)));
    CompMu(ix) = .5*(L(k) + R(k));
    
    TotP = pdf(k).proportion'*CompP;
    
    muh(k) = (1/TotP)*( pdf(k).proportion'*(CompP.*CompMu));
end

