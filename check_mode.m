function pass = check_mode(source, target)

f = fieldnames(target);
pass = 1;
for k=1:length(f)
    if ~isfield(source, f{k})
        pass = 0;
        %warning('Trained models are from older codec version.')
        return 
    end
    if ~strcmp(class(source.(f{k})),class(target.(f{k})))
        pass = 0;
        return
    end
    switch class(target.(f{k}))
        case 'char'
            if ~strcmp(source.(f{k}),target.(f{k}))
                pass = 0;
                return
            end
        case {'double', 'logical'}
            foo = source.(f{k});
            bar = target.(f{k});
            if (numel(bar) ~= numel(foo)) || (norm(foo(:)-bar(:)) > eps)
                pass = 0;
                return
            end
    end
end


