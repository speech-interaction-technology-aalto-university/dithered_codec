function evaluate_codec(parameter_list)

if length(dbstack) == 1
    disp('Evaluate dithered codec')
    disp('   Default configuration:')
end

parameter_count = size(parameter_list,1);

% default configuration
[codec_mode,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters();


for ix=1:parameter_count
    if strcmp(class(parameter_list{ix,2}),'char')
        this_argnum = 1;
    else
        this_argnum = length(parameter_list{ix,2});
    end
   if this_argnum == 1
       codec_mode.(parameter_list{ix,1}) = parameter_list{ix,2};
       
       switch parameter_list{ix,1}
           case {'envelope_order', 'envelope_mode', 'envelope_covariance_diagonal'}
               codec_mode = generate_envelope_model(codec_mode);
       end
   else
       for k=1:this_argnum
           parameter_list_excerpt = parameter_list;
           switch class(parameter_list{ix,2})
               case {'double','logical'}
                   parameter_list_excerpt{ix,2} = parameter_list{ix,2}(k);
               case 'cell'
                   parameter_list_excerpt{ix,2} = parameter_list{ix,2}{k};
           end
           evaluate_codec(parameter_list_excerpt);
       end
       return
   end
end

s = 'Evaluating parameter: ';
for k=1:size(parameter_list,1)
    if k>1, s = [s ', ']; end
    s = [s parameter_list{k,1} ' '];
    switch class(parameter_list{k,2})
        case {'double','logical'}
            s = [s num2str(parameter_list{k,2}) ' '];
        case {'char'}
            s = [s parameter_list{k,2}];
    end    
end
disp(s)

EnvStat = load_statistics(codec_mode,envstat_filename);
[d_train,d_test,filenum_train,filenum_test] = extract_filenames(timit_path,temp_path);


if 1
    %warning('Reduced evaluation set')
    disp('   Warning: Reduced evaluation set in evaluate_codec()')
    filenum_test = 100;
end

snr_file_list = zeros(filenum_test,1);
snrvector = [];

waith = waitbar(0,'Encoding files');
for fileix= 1:filenum_test
    waitbar(fileix/filenum_test,waith);
    
    rng(1) % set random seed for reproducibility
    
    x=readsph(d_test{fileix},[],-1,-1);
    
    [xq,snr_file] = dithered_codec(x,codec_mode,runtime_mode,EnvStat);
    snr_file_list(fileix) = mean(snr_file);
    snrvector = [snrvector; snr_file(:)];
    
    
    
    if 0
        disp(['Mean perceptual SNR ' num2str(mean(snr_file)) ' dB'])
        soundsc([x; xq],codec_mode.sampling_frequency)
        pause(2*length(x)/codec_mode.sampling_frequency)
    end    
    
    if 0
        clf        
        hist(snrvector,100)
        %waitforbuttonpress
        drawnow
    end
    
end
close(waith);
if 1
    disp(['   Mean SNR ' num2str(mean(snrvector)) ' dB']);
end

s = generate_evaluation_file_name(temp_path,parameter_list);
save(s,'snrvector','snr_file_list')
