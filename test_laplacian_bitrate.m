clear

N = 10^6;
x = randl(N,1);

[h,ix] = hist(x,floor(min(x)):ceil(max(x)));

clf
plot(ix,h)

p = h/N;
b = -log2(p+eps);
bitrate = sum(p.*b);

[bitrate log(2*exp(1))/log(2)]

plot(ix,laplacian_cdf(ix))