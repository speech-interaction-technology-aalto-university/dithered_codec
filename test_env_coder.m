clear

disp('Test envelope coder for dithered coding experiments')

generate_parameters


load([temp_path 'Xmat.mat'])
load([temp_path 'EnvStat.mat'])
load([temp_path 'dq.mat'])

wincnt = size(Xmat,2);

B = zeros(wincnt,1);
for k=1:wincnt
    
    Env = EnvMtx*20*log10(abs(Xmat(:,k)));
    Ql = floor(Env/dq)*dq;
    Qu = ceil(Env/dq)*dq;
    Cl = normcdf(Ql,EnvMu,EnvStd);
    Cu = normcdf(Qu,EnvMu,EnvStd);
    
    bvec = -log2(Cu-Cl);
    B(k) = sum(bvec);
    
    if 0
        clf
        plot(bvec)
        waitforbuttonpress
    end
    
end


hist(B,1000);
