function muh = expectation_truncated_normal(L,R,mu,s)

%phi = @(x) normpdf(x,0,1); 
phi = @(x) (1/sqrt(2*pi))*exp(-(x.^2)/2);
%Phi = @(x) normcdf(x,0,1); 
Phi = @(x) 0.5*(1 + erf(x/sqrt(2)));

alpha = (L-mu)./s;
beta = (R-mu)./s;

muh =mu - s.* (phi(beta)-phi(alpha))./(eps+Phi(beta)-Phi(alpha));
