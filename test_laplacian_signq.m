clear

N = 10^6;
x = randl(N,1);


gainfn = @(x,ref) (x(:)'*ref(:))/(x(:)'*x(:));

xq = sign(x);

gainfn(xq,x)