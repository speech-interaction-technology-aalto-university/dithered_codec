function c = laplacian_cdf(x)

c = x;
ix = find(x < 0);
c(ix) = 0.5 * exp(x(ix)/sqrt(.5));
ix = find(x >=0);
c(ix) = 1 - 0.5*exp(-x(ix)/sqrt(.5));
