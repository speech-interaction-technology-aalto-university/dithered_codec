function x = randl(N,M)


u = rand(N,M);

x = sign(u-0.5).*log(1 - 2*abs(u - 0.5))/sqrt(2);
