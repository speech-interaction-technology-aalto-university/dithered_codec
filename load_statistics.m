function EnvStat = load_statistics(codec_mode,envstat_filename,no_training)

if nargin < 3
    no_training = 0;
end

stats_found = 0;
if numel(dir(envstat_filename)) > 0
    load(envstat_filename,'EnvStat')
    
    EnvStat = get_mode(EnvStat,codec_mode);    
    if isstruct(EnvStat) && isfield(EnvStat,'specstd') && length(EnvStat.specstd)
        stats_found = 1;
    end
end

if (stats_found == 0) 
    if (no_training == 0)
        disp('Training data not found.')

        train_all(codec_mode)

        [~,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters();
        [d_train,d_test,filenum_train,filenum_test] = extract_filenames(timit_path,temp_path);


        load(envstat_filename,'EnvStat')
        EnvStat = get_mode(EnvStat,codec_mode);

    else
        if ~exist('EnvStat')
            EnvStat = struct('codec_mode',codec_mode);
        end
    end
end


% Envelope pdf - calculate constants
if (~codec_mode.envelope_covariance_diagonal || ~isfield(EnvStat,'CovDet')) ...
        && isfield(EnvStat,'GMModel')
    EnvStat.pdf = EnvStat.GMModel{codec_mode.envelope_mixture_order};
    for k=1:codec_mode.envelope_mixture_order
        EnvStat.iCov(:,:,k) = pinv(EnvStat.pdf.Sigma(:,:,k));
        for h=1:codec_mode.envelope_order
            EnvStat.CovDet(k,h) = det(EnvStat.pdf.Sigma(1:h,1:h,k));
        end
    end
end
