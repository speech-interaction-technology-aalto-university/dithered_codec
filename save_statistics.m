function save_statistics(codec_mode,envstat_filename,EnvStat_this)

if numel(dir(envstat_filename))
    load(envstat_filename)
    [~,ix] = get_mode(EnvStat,codec_mode);
    if ix == -1
        ix = length(EnvStat)+1;
    end
    f = fieldnames(EnvStat_this);
    for k=1:length(f)
        %eval(['EnvStat(ix).' f{k} ' = getfield(EnvStat_this,f{k});']);        
        EnvStat(ix).(f{k}) = EnvStat_this.(f{k});
    end
else
    EnvStat = EnvStat_this;
end
save(envstat_filename,'EnvStat');
