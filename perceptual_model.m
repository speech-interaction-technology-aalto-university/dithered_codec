function W = perceptual_model(X,codec_mode)
% function W = perceptual_model(X,fs)
% Generate perceptual model for the spectrum X with sampling rate fs
% Assumes that

if nargin < 1
    warning('Test mode')
    fs = 16000;
    X = zeros(320,1);
    ix1k = round(320*(1000/(.5*fs)));
    X(ix1k) = 60;
end
fs = codec_mode.sampling_frequency;
speclen = size(X,1);

switch codec_mode.perceptual_model_mode
    case 'ACELP'
        % EVS derived
        xwinh = codec_mode.MdctMtx'*X;
        a = lpc(xwinh.*codec_mode.hwin,16);
        aw = (0.92.^(0:16)).*a;
        w = conv([1 .68],aw);
        W = 1./abs(fft(w,speclen*2)); W = W(1:speclen)';
    case 'homebrew'

        speclen = length(X);
        W = zeros(size(X));


        % Constant part (dB)

        tilt_start_Hz = 500;
        tilt_start = round(speclen*tilt_start_Hz/(.5*fs))+1;
        tilt_end_Hz = fs/2;

        ix = (tilt_start:speclen)';
        W = ones(size(X));
        %W(ix) = 10.^((3*(log10(ix-1) -log10(ix(1)-1))/(log10(1000)-log10(500)))/20);
        W(ix) = linspace(1,10.^((3*(log10(ix(end)-1) -log10(ix(1)-1))/(log10(1000)-log10(500)))/20),length(ix)).^2;


        % Signal dependent part 
        aX = abs(X);
        dk = speclen*100/(fs/2);
        foo = zeros(size(aX));
        for k=1:speclen
            foo(k) = max(aX(max(1,(k-dk)):min(speclen,k+dk)));
        end
        aX = foo;
        for k=2:speclen
            if aX(k) < aX(k-1)
                aX(k) = aX(k-1)*.9 + aX(k);
            end
        end
        for k=speclen-1:-1:1
            if aX(k) < aX(k+1)
                aX(k) = foo(k+1)*.8 + aX(k);
            end
        end

        W = W.*aX;
        
    otherwise
        error(['Unknown perceptual model :' codec_mode.perceptual_model_mode])
end


if nargin < 1 % plots for testing

    clf
    plot((0:speclen-1)*fs/(2*(speclen-1)),W)
    grid on
    
end