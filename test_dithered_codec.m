function test_dithered_codec(paramlist);

disp('Test envelope coder for dithered coding experiments')


[codec_mode,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters();
[d_train,d_test,filenum_train,filenum_test] = extract_filenames(timit_path,temp_path);
EnvStat = load_statistics(codec_mode,envstat_filename);


if nargin > 0
    runtime_mode = runtime_configuration(codec_mode,paramlist);
end

% code random files
for fileix= 1%randperm(filenum_test)
    
    rng(1) % set random seed for reproducibility
    
    x=readsph(d_test{fileix},[],-1,-1);
    
    [xq,snr] = dithered_codec(x,codec_mode,runtime_mode,EnvStat);
    
    disp(['Mean perceptual SNR ' num2str(mean(snr)) ' dB'])
    if 1
        soundsc([xq],codec_mode.sampling_frequency)
        pause(length(x)/codec_mode.sampling_frequency)
    elseif 1
        
        soundsc([x; xq],codec_mode.sampling_frequency)
        pause(2*length(x)/codec_mode.sampling_frequency)
    end    
    
end