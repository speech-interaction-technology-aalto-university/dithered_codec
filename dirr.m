function [filelist,totsize] = dirr(path,wilcard)

dirlist{1} = path;
ix = 0;
fix = 0;
totsize = 0;
filelist = {};

while ix < length(dirlist)
    ix = ix+1;
    %disp(dirlist{ix});
    d = dir([dirlist{ix}]);
    for k=1:length(d)
        %disp(d(k))
        if d(k).isdir && d(k).name(1)~='.'
            dirlist{end+1} = [dirlist{ix} d(k).name filesep];
        elseif ~isempty(regexp(d(k).name,wilcard)) 
            fix = fix +1;
            filelist{fix} = [dirlist{ix} d(k).name];
            totsize = totsize + d(k).bytes;
            %disp(filelist{fix});
        end
    end
end
    