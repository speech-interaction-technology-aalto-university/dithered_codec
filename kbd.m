function w = kbd(L,alpha)
% function w = kbd(L,alpha)

wk = kaiser(L/2+1,alpha);
wk = wk/sum(wk);
w = sqrt(cumsum(wk(1:end-1)));
w = [w(:); flipud(w(:))];
