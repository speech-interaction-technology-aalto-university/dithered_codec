function A = randrot(N,method)
% function A = randrot(N,method)
% Orthonormal random rotation matrix of size NxN 

if nargin < 2
    method = 'directqr';
end

switch lower(method)
    case 'directqr'
        A = sign(randn)*eye(N);
        for h=1:N
            q = randn(N,1); q = q/sqrt(sum(q.^2));
            A = A - 2*q*(q'*A);
        end
        
    case 'directqrx'
        A = sign(randn)*eye(N);
        for h=1:round(N*1.3)
            q = randn(N,1); q = q/sqrt(sum(q.^2));
            A = A - 2*q*(q'*A);
        end
    case 'directqr_cplx'
        A = exp(i*2*pi*rand)*eye(N);
        for h=1:N
            q = randn(N,1)+1i*randn(N,1); q = q/sqrt(sum(abs(q).^2));
            A = A - 2*q*(q'*A);
        end
        
    case 'directqrx_cplx'
        A = exp(i*2*pi*rand)*eye(N);
        for h=1:round(N*1.3)
            q = randn(N,1)+1i*randn(N,1); q = q/sqrt(sum(abs(q).^2));
            A = A - 2*q*(q'*A);
        end
    case 'qr'
        [A,~] = qr(randn(N));
        A = sign(randn)*A;
    case 'sort'
        [~,ix] = sort(randn(N,1));
        I = eye(N);
        A = I(:,ix);
        if 1
            A = diag(sign(randn(N,1)))*A;
        else
            if mod(N,2)
                P = zeros(N);
                foo = randn(1,2); foo = foo/norm(foo); foo = [foo; -foo(2) foo(1)];
                P(1:2,1:2) = foo;
                for pn = 3:2:N-3
                    P(pn+(0:1),pn+(0:1)) = P(1:2,1:2);
                end
                [P(N-(0:2),N-(0:2)),~] = qr(randn(3));
                A = P*A;
            else
                P = zeros(N);
                foo = randn(1,2); foo = foo/norm(foo); foo = [foo; -foo(2) foo(1)];
                P(1:2,1:2) = foo;
                for pn = 3:2:N
                    P(pn+(0:1),pn+(0:1)) = P(1:2,1:2);
                end
                A = P*A;            
            end
        end
        
    case 'fastsubgroup'
        K = N;
        A = eye(N);
        while K>1
            
            k=0;
            while k <= N-2*K
                x = randn(K,1); x = x/sqrt(sum(x.^2));
                P = eye(K) - 2*x*x';    
                B = eye(N);
                ix = k + (1:K);
                B(ix,ix) = P;
                A = B*A;
                k = k+K;
            end
            Kh = N-k;
            %x = [x; randn(Kh-K,1)]; x = x/sqrt(sum(x.^2));
            x = randn(Kh,1); x = x/sqrt(sum(x.^2));
            P = eye(N-k) - 2*x*x';
            ix = k + (1:Kh);
            B = eye(N);
            B(ix,ix) = P;
            A = B*A;
            K = floor(K/2);
        end            
    case 'subgroup'
        A = sign(randn);
        for k=2:N
            %A(k,k) = sign(randn);
            A = [1 zeros(1,k-1); zeros(k-1,1) A];
            x = randn(k,1); x=x/norm(x);
            P = eye(k) - 2*x*x';
            A = P*A;
        end
    case 'brute'
        if 0
            A = eye(N);
            for k=1:N-1
                for h=(k+1):N
                    Q = eye(N);
                    Q([h k],[h k])=randrot2;
                    %A([h k],[h k]) = randrot2*A([h k],[h k]);
                    A = A*Q;
                end
            end
        else
            A = 1;
            for k=2:N
                w = randn(k,1);
                A = [w null(w')]*[A zeros(k-1,1); zeros(1,k-1) 1];
            end
        end
    case 'realbrute'
        A = eye(N);
        for k=1:N-1
            for h=(k+1):N
                Q = eye(N);
                Q([h k],[h k])=randrot2r;
                %A([h k],[h k]) = randrot2*A([h k],[h k]);
                A = Q*A;
            end
        end
    case 'realsuperfast'
        switch N
            case 1
                A = 1;
            case 2
                A = randrot2r();
            otherwise
                N2a = floor(N/2);
                N2b = N-N2a;
                A = [randrot(N2a) zeros(N2a,N2b); zeros(N2b,N2a) randrot(N2b)];
                for k=1:N2a
                    B = eye(N);
                    ix = k+[0 N2a];
                    B(ix,ix) = randrot2r();
                    A = B*A;
                end
                if N2a < N2b
                    B = eye(N);
                    ix = [ceil(rand*(N-1)); N];
                    B(ix,ix) = randrot2r();
                    A = B*A;
                    
                end
        end
    case 'realsuperfastreduced'
        switch N
            case 1
                A = 1;
            case 2
                A = randrot2r();
            otherwise
                N2a = floor(N/2);
                N2b = N-N2a;
                a = 2*pi*rand;
                A = [randrot(N2a) zeros(N2a,N2b); zeros(N2b,N2a) randrot(N2b)];
                I = eye(N2a);
                B = [cos(a)*I sin(a)*I; -sin(a)*I cos(a)*I];
                A = A*B;
                if N2a < N2b
                    B = eye(N);
                    ix = [ceil(rand*(N-1)); N];
                    B(ix,ix) = randrot2r();
                    A = A*B;
                    
                end
        end
    case 'realsuperfastn'
        switch N
            case 1
                A = 1;
            case 2
                A = randrot2r();
            otherwise
                if mod(log2(N),1)
                    error('N must be a power of two')
                end
                a = rand(ceil(log2(N)),1)*2*pi;
                A = 1;
                for k=1:log2(N)
                    A = [cos(a(k))*A sin(a(k))*A; -sin(a(k))*A cos(a(k))*A];
                end
                
                
        end
    case 'realwhite'
        w = randn(N,1);
        A = recrot(w)*diag(sign(randn(N,1)));                
    otherwise
        error(['Unknown method: ' method])
end




function A2 = randrot2r()
s = rand*2*pi;

a1 = cos(s); a2 = sin(s);
A2 = [a1 a2; -a2 a1];

function A2 = randrot2()
s = rand*2*pi;
t = rand(2,1)*2*pi;

a1 = cos(s); a2 = sin(s);
A2 = [a1 a2; -a2 a1]*diag(exp(i*t));
%abs(eig(A2))


function A = recrot(w)

   N = length(w);
   switch N
       case 2
           s = w/norm(w);
           A = diag(sign(randn(2,1)))*[s(1) s(2); -s(2) s(1)]*diag(sign(randn(2,1)));
       case 3
           s = w(2:3)/norm(w(2:3));
           A = [1 0 0; 0 s(1) s(2); 0 -s(2) s(1)]*diag([1 sign(randn(1,2))]); 
           w1 = A*w;
           s = w1(1:2)/norm(w1(1:2));
           B = [s(1) s(2); -s(2) s(1)]*diag(sign(randn(2,1))); 
           B(3,3) = 1;
           A = B*A;
           w = A*w;
       otherwise
           N2a = floor(N/2); N2b = N-N2a; Nc = N2b-N2a;
           A = diag(sign(randn(N,1)))*[recrot(w(1:N2a)) zeros(N2a,N2b); zeros(N2b,N2a) recrot(w((N2a+1):N))];
           u = A*w;
           s = u([1 N2a+1])/norm(u([1 N2a+1]));
           A = [s(1)*eye(N2a) s(2)*eye(N2a) zeros(N2a,Nc); -s(2)*eye(N2a) s(1)*eye(N2a) zeros(N2a,Nc); zeros(Nc,2*N2a) ones(Nc)]*A;
   end
   
   return
   