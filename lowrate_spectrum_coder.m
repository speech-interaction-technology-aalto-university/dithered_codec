function [Xwq,bits_1bit] = lowrate_spectrum_coder(Xw,bits,codec_mode,target_bits)

bits_1bit = min(length(Xw),floor(bits));

if (bits_1bit == 0) || (sum(target_bits)==0) || (length(Xw)==0)
    Xwq = zeros(size(Xw));
    return
end
len = length(Xw);

switch codec_mode.quantizer_1bit_mode
    case 'randomrotation'
        P = randrot(length(Xw),'qr');
        foo = P*Xw;        
        fooq = zeros(size(foo));
        fooq(1:bits_1bit) = sqrt(2/pi)*sign(foo(1:bits_1bit));
        %fooq((bits_1bit+1):end) = .3*randn(length(ixlo)-bits_1bit,1);
        Xwq = P'*fooq;
    case 'noiseshaping'
        sumt = sum(target_bits);
        if sumt > bits_1bit
            % normalize downwards
            target_bits = target_bits(:)*bits_1bit/sumt;
        elseif sumt < bits_1bit
            % compress upwards to 1
            
            
            %bits_1bit = (sumt*g + len)/(len+g);
            g =  (len - bits_1bit)/(bits_1bit - sumt);
            target_bits = (len+g*target_bits(:))/(len+g);
            
        end
        
        target_error = ones(len,1)-target_bits*sqrt(2/pi);
        %target_error = ones(len,1)-((2.^target_bits) -1)*sqrt(2/pi);
        source_error = [ones(bits_1bit,1)*(1-sqrt(2/pi)); ones(len-bits_1bit,1)];
        [target_error,sortix] = sort(target_error);
        interpolation_sequence = find_interpolation_sequence(target_error,source_error);
        foo = rotation_sequence(Xw(sortix),interpolation_sequence,1);
        fooq = zeros(size(foo));
        fooq(1:bits_1bit) = sqrt(2/pi)*sign(foo(1:bits_1bit));
        Xwq(sortix) = rotation_sequence(fooq,interpolation_sequence,-1);
        
end


function x = rotation_sequence(x, sequence, d)

for k=1:size(sequence,1)
    t = sequence(k,1);
    L = sequence(k,2);
    R = sequence(k,3);
    s = sqrt(abs(1-t));
    t = sqrt(abs(t));
    x([L R]) = [t -d*s; d*s t]*x([L R]);
end







function sequence = find_interpolation_sequence(target,source)


N = length(target);
L = 1;
R = N;

sequence = zeros(N-1,3);

for k=1:N-1
    
    %[target(L)-source(L), source(R)-target(R)]
    if target(L)-source(L) > source(R)-target(R)
        % delta R is smaller
        t = abs((source(L)-target(R))/(eps+source(L)-source(R)));
        sequence(k,:) = [t L R];        
        source([L R]) = [t*source(L) + (1-t)*source(R); (1-t)*source(L) + t*source(R)];

        R = R-1;
    else
        % delta L is smaller
        t = abs(1-(source(L)-target(L))/(eps+source(L)-source(R)));
        sequence(k,:) = [t L R];        
        source([L R]) = [t*source(L) + (1-t)*source(R); (1-t)*source(L) + t*source(R)];

        L = L+1;
    end
    
    
end