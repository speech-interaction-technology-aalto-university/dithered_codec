function EnvStat_match = load_matching_statistics(codec_mode,envstat_filename,mode_list,nonempty)

EnvStat_match = [];

if length(dir(envstat_filename))
    load(envstat_filename,'EnvStat');
    for EnvIx = 1:length(EnvStat)
        match = 1;
        if ~isstruct(EnvStat(EnvIx).codec_mode)
            continue % sanity check
        end
        for k=1:length(mode_list)
            switch class(codec_mode.(mode_list{k}))
                case 'char'
                    if ~strcmp(EnvStat(EnvIx).codec_mode.(mode_list{k}), codec_mode.(mode_list{k}))
                        match = 0;
                        continue
                    end                    
                otherwise
                    if EnvStat(EnvIx).codec_mode.(mode_list{k}) ~= codec_mode.(mode_list{k})
                        match = 0;
                        continue
                    end
            end
        end
        if (nargin > 3) 
            for zz = 1:length(nonempty)
                if (~isfield(EnvStat(EnvIx),nonempty{zz}) || ~length(EnvStat(EnvIx).(nonempty{zz})) )
                    match = 0;
                end
            end
        end
        if match
            EnvStat_match = EnvStat(EnvIx);
            break
        end
    end
end