function mux = expectation2_truncated_laplacian(L,R,mu,s)

Lh  = (L-mu)./s;
Rh = (R-mu)./s;
absL = sqrt(2)*abs(Lh);
absR = sqrt(2)*abs(Rh);
expL = exp(-absL);
expR = exp(-absR);
x2fxL = (0.5)*sign(Lh).*(1 - expL.*(Lh.^2 + absL + 1));
x2fxR = (0.5)*sign(Rh).*(1 - expR.*(Rh.^2 + absR + 1));
cL = 0.5*sign(L).*(1-expL);
cR = 0.5*sign(R).*(1-expR);

mux = mu + s*sign(R+L).*sqrt((x2fxR - x2fxL)./(eps + cR - cL));

% sanity
mux = max(mux,L);
mux = min(mux,R);