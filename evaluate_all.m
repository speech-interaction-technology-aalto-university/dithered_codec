clear

disp('Running all entropy evaluations')


evaluate_codec({'envelope_order',4:2:22});
evaluate_codec({'envelope_mode',{'mfcc', 'cepstrum', 'triangular_uniform', 'mfcc2', 'sfb_uniform', 'sfb_mel'}});
evaluate_codec({'envelope_covariance_diagonal',[true false]});
evaluate_codec({'envelope_exponent',[1:.25:2]});
evaluate_codec({'envelope_mixture_order',1:5});
evaluate_codec({'envelope_quantization_target_dB',[.8:.2:1.6 1.7:.1:2.0 2.2]});
evaluate_codec({'spectrum_distribution',{'laplacian','logistic mixture'}});
evaluate_codec({'dithering',[true false]});
evaluate_codec({'quantizer_1bit',[true false]});
evaluate_codec({'quantizer_1bit_mode',{'randomrotation','noiseshaping'}});
