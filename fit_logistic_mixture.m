function pdf = fit_logistic_mixture(x,mixture_components)
% function pdf = fit_logistic_mixture(x,mixture_components)

x = x(:);
maxiteration = 200;

[h,hix] = hist(x,300);

for mc = mixture_components
    
    % initial guess
    %mu = 2*(.5:mc)'/mc - 1;
    mu = 2*rand(mc,1) - 1;
    s = ones(mc,1)/mc;
    s2 = ones(mc,1)/mc;
    proportion = ones(mc,1)/mc;
    
    for iteration = 1:maxiteration
        
        % find likelihood of each component for each x
        c = zeros(size(x,1),mc);
        for comp = 1:mc
            p(:,comp) = 1./(1 + exp(-(x-mu(comp))/s(comp)));
        end
        p = (p.*(1-p))*diag(proportion./s);
        
        % posteriori mean and scaling
        for comp = 1:mc
            mu(comp) = sum(p(:,comp).*x)/sum(p(:,comp));
            s2(comp) = sum(p(:,comp).*((x - mu(comp)).^2))/sum(p(:,comp));            
        end
        s = sqrt(s2*3/pi);
        
        % posteriori component likelihoods 
        p = p.*((1./sum(p')')*ones(1,mc));
        proportion = mean(p,1)';
        proportion = proportion/sum(proportion);
        
        
        clf
        plot(hix,h/((hix(2)-hix(1))*sum(h)));
        hold on
        for comp=1:mc
            f(comp,:) = 1./(1 + exp(-(hix-mu(comp))/s(comp)));
            f(comp,:) = (proportion(comp)./s(comp))*(f(comp,:).*(1-f(comp,:)));            
        end
        f(mc+1,:) = sum(f(1:comp,:),1);
        plot(hix, f)
        title(num2str([mc iteration maxiteration]))
        drawnow
    end
    
end

pdf = [];