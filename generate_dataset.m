function generate_dataset(codec_mode)


force_generation = 0;

disp('Checking dataset for dithered coding experiments')
disp('  Dataset: TIMIT')

[~,runtime_mode,timit_path,temp_path] = generate_parameters();
[d_train,d_test,filenum_train,filenum_test] = extract_filenames(timit_path,temp_path);

window_length = codec_mode.window_length;
window_step = codec_mode.window_step;
hwin = codec_mode.hwin;
speclen = codec_mode.speclen;
MdctMtx = codec_mode.MdctMtx;


%% Training data
try    
    if force_generation, error(' '), end
    if numel(dir([temp_path 'Xmat.mat'])) == 0, error(' '), end
    %load([temp_path 'Xmat.mat'],'Xmat')
    %disp('  Loaded')    
catch    
    disp('Training data')

    wh = waitbar(0,'Count number of training frames');
    Xix = 0;
    for fileix = 1:filenum_train
        
        x_list = read_short_timit(d_train{fileix});
        
        for seg=1:length(x_list)
            winnum = floor((length(x_list{seg})-window_length)/window_step)+1;
            Xix = Xix+winnum;
        end
        waitbar(fileix/filenum_train,wh)
        
    end
    close(wh)
    totwinnum = Xix;
    disp(['  Training frame count ' num2str(totwinnum)])
    
    wh = waitbar(0,'Generating training set');
    Xmat = zeros(speclen,totwinnum); Xix = 0;
    for fileix = 1:filenum_train
        
        x_list = read_short_timit(d_train{fileix});
        
        for seg=1:length(x_list)
            winnum = floor((length(x_list{seg})-window_length)/window_step)+1;
            
            %Xmat = [Xmat zeros(envelope_order,winnum)];
            for k=1:winnum
                ix = (k-1)*window_step + (1:window_length);
                xwin = x_list{seg}(ix).*hwin;
                
                X = MdctMtx*xwin;
                Xix = Xix+1;
                Xmat(:,Xix) = X;
            end
        end
        waitbar(fileix/filenum_train,wh)
        
    end
    close(wh)
        
    save([temp_path 'Xmat.mat'],'Xmat')
    disp('  Saved')
end
    

%% Training data
try    
    if force_generation, error(' '), end
    if numel(dir([temp_path 'Xmat_test.mat'])) == 0, error(' '), end
    %load([temp_path 'Xmat_test.mat'],'Xmat')
    %disp('  Loaded')
catch    
    disp('Test data')
    
    wh = waitbar(0,'Count number of test frames');
    Xix = 0;
    for fileix = 1:filenum_test
        
        x_list = read_short_timit(d_test{fileix});
        
        for seg=1:length(x_list)
            winnum = floor((length(x_list{seg})-window_length)/window_step)+1;
            Xix = Xix+winnum;
        end
        waitbar(fileix/filenum_test,wh)
        
    end
    close(wh)
    totwinnum = Xix;
    disp(['  Test frame count ' num2str(totwinnum)])
    
    wh = waitbar(0,'Generating test set');
    Xmat = zeros(speclen,totwinnum); Xix = 0;
    for fileix = 1:filenum_test
        
        x_list = read_short_timit(d_test{fileix});
        
        for seg=1:length(x_list)
            winnum = floor((length(x_list{seg})-window_length)/window_step)+1;
            
            %Xmat = [Xmat zeros(envelope_order,winnum)];
            for k=1:winnum
                ix = (k-1)*window_step + (1:window_length);
                xwin = x_list{seg}(ix).*hwin;
                
                X = MdctMtx*xwin;
                Xix = Xix+1;
                Xmat(:,Xix) = X;
            end
        end
        waitbar(fileix/filenum_test,wh)
        
    end
    close(wh)

    save([temp_path 'Xmat_test.mat'],'Xmat')
    disp('  Saved')
end
    