function train_spectral_bias(codec_mode,force)

if length(dbstack) == 1
    disp('Train envelope coder for dithered coding experiments')
end
disp('  Spectral magnitude bias')

if nargin < 2
    force = 0;
end

[~,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters();
[d_train,d_test,filenum_train,filenum_test] = extract_filenames(timit_path,temp_path);

EnvStat = load_statistics(codec_mode,envstat_filename,true);
if ~force
    if isfield(EnvStat,'spectral_pdf') && length(EnvStat.spectral_pdf)
        return % already trained
    end
end


EnvFn = codec_mode.EnvFn;
iLogEnvFn = codec_mode.iLogEnvFn;
speclen = codec_mode.speclen;
bits_per_frame = runtime_mode.bits_per_frame;

load([temp_path 'Xmat.mat'])

if 1
    disp('   Warning: Limited training set enabled for speed in train_spectral_bias()')
    Xmat = Xmat(:,randperm(size(Xmat,2),10000));
end

dq_env = EnvStat.dq_env;


bits_env = zeros(size(Xmat,2),1);
yq = zeros(size(Xmat));
waith = waitbar(0,'Encoding');
for k=1:size(Xmat,2)
    if mod(k,100)==0, waitbar(k/size(Xmat,2),waith), end
    Env = EnvFn(Xmat(:,k));

    Ql = floor(Env/EnvStat.dq_env)*EnvStat.dq_env;
    Qu = ceil(Env/EnvStat.dq_env)*EnvStat.dq_env;
        
    [bits_env(k),Envq] = envelope_coder(Ql,Qu,EnvStat,codec_mode,bits_per_frame);
    yq(:,k) = iLogEnvFn(Envq);
end
close(waith)

Xmat = Xmat.*(10.^(-yq/20));
clear yq

specstd = std(Xmat,0,2);
EnvStat.specstd = specstd;
%save([temp_path 'specstd.mat'],'specstd')

clf
plot(specstd)
drawnow

if 0
    EnvStat_this = EnvStat;
    load(envstat_filename)
    [~,ix] = get_mode(EnvStat,codec_mode);
    f = fieldnames(EnvStat_this);
    for k=1:length(f)    
        %eval(['EnvStat(ix).' f{k} ' = getfield(EnvStat_this,f{k});']);
        EnvStat(ix).(f{k}) = getfield(EnvStat_this,f{k});
    end
    save(envstat_filename,'EnvStat');
end

Xmat = diag(1./specstd)*Xmat;




%%
disp('  Spectral component entropy estimates')
dq = 2.^-8;
Xmin = dq*floor(min(Xmat(:))/dq);
Xmax = dq*ceil(max(Xmat(:))/dq);
hix = Xmin:dq:Xmax;
spec_bits = zeros(speclen,1);
for k=1:speclen %randperm(speclen)
    
    h = hist(Xmat(k,:),hix);
    p = h/sum(h);
    
    spec_bits(k) = log2(dq)-sum(p.*log2(eps+p));
end

EnvStat.spectral_bits = spec_bits;
%save(envstat_filename,'EnvStat')


%%
disp('  Spectral component distributions')



clf
options = statset('MaxIter',2000);
sign_gain = zeros(speclen,1);
spectral_pdf = struct();
for k=1:speclen %randperm(speclen)
    
    [h,hix] = hist(Xmat(k,:),200);
    
    neglog = zeros(5,1)+NaN; aic = neglog; b = neglog;
    for mc=1:3
        try
            if k == 1
                GMModel = fitgmdist(Xmat(k,:)',mc,'options',options);
            else
                guess = struct(...
                'mu',spectral_pdf(k-1,mc).mu,...
                'Sigma',reshape(spectral_pdf(k-1,mc).s.^2,[1 1 mc]),...
                'ComponentProportion',spectral_pdf(k-1,mc).proportion);

                GMModel = fitgmdist(Xmat(k,:)',mc,'options',options,'Start',guess);
            end
        catch
            GMModel = fitgmdist(Xmat(k,:)',mc,'options',options,'RegularizationValue',0.1);
        end
        neglog(mc) = GMModel.NegativeLogLikelihood;
        aic(mc) = GMModel.AIC;
        
        s = zeros(mc,1); mu = s; proportion = s;
        f = [];
        for comp=1:mc
            mu(comp) = GMModel.mu(comp);        
            proportion(comp) = GMModel.ComponentProportion(comp);
            %s= sqrt(v*3)/pi
            s(comp,1) = sqrt(3*GMModel.Sigma(:,:,comp))/pi; 
            f(comp,:) = 1./(1 + exp(-(hix-mu(comp))/s(comp)));
            f(comp,:) = (proportion(comp)./s(comp))*(f(comp,:).*(1-f(comp,:)));    
        end
        f(mc+1,:) = sum(f(1:comp,:),1);
        b(mc,1) = -sum(f(end,:).*log2(eps+f(end,:)));
        
        spectral_pdf(k,mc).mu = mu;
        spectral_pdf(k,mc).s = s;
        spectral_pdf(k,mc).proportion = proportion;
        spectral_pdf(k,mc).neglog = GMModel.NegativeLogLikelihood;
        spectral_pdf(k,mc).aic = GMModel.AIC;

        if mc > 1
            clf            
            subplot(221)            
            plot(hix,h/((hix(2)-hix(1))*sum(h)),'LineWidth',2)
            hold on
            plot(hix, f)
            axis tight
            title('Training spectral component distributions')

            subplot(222)
            plot(aic)        
            title(['AIC at ' num2str([k mc])])
            subplot(223)
            plot(neglog)        
            title(['NegLogLike at ' num2str([k mc])])
            subplot(224)
            %plot([neglog/(size(Xmat,2)*log(2)) b+log2(hix(2)-hix(1))])
            plot(b)
            title('bits')
            drawnow
        end
    end
end

%EnvStat(ix).sign_gain = sign_gain;
EnvStat.spectral_pdf = spectral_pdf;
%save(envstat_filename,'EnvStat')



save_statistics(codec_mode,envstat_filename,EnvStat);
