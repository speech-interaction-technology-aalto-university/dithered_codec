function [x,fs] = read_short_timit(filename)

[y,fs,ffx]=readsph(filename,[],-1,-1);
try
    phon = readphn([filename(1:end-3) 'PHN']);
catch
    phon = readphn([filename(1:end-3) 'phn']);
end
x = remove_timit_silence(y,phon);