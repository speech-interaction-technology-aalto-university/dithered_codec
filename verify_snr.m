clear


N = 10000;

x = rand(N,1);
xq0 = 0.5*ones(N,1);
xq1 = 0.75*ones(N,1);
ix = find(x < 0.5);
xq1(ix) = 0.25;

disp(['Improvement in dB ' num2str(-diff(20*log10([norm(x-xq0) norm(x-xq1)])))])
