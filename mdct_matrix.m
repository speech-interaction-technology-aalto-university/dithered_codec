function A = mdct_matrix(N)
% function A = mdct_matrix(N)

if rem(N,2)
    error('Window length must be even.')
end

k = 0:((N/2)-1);
n_0 = (N/2+1)/2;

A1 = zeros(length(k));
A2 = A1;

for n=0:(N/2)-1
    A1(:,n+1) = cos((2*pi/N)*(n + N/2 + n_0)*(k+.5))';
    A2(:,n+1) = -cos((2*pi/N)*(n + n_0)*(k+.5))';
end

A = [A1 A2]/sqrt(N/2);
