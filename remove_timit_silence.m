function y = remove_timit_silence(x,phon)

ix = [];
for k=1:length(phon)
    switch(phon(k).name)
        case {'h#','pau','epi'}
            ix = [ix; phon(k).start phon(k).end];            
    end
end
for k = 1:size(ix,1)-1
    y{k} = x(ix(k,2):ix(k+1,1));
end