function c = fast_normcdf(x,mu,sigma)
% c = fast_normcdf(x,mu,sigma)

s = sqrt(3)/pi;
c = 1./(1 + exp(-(x-mu)./(s*sigma)));