clear

disp('Test envelope coder for dithered coding experiments')


generate_parameters
extract_filenames

try
    load(envstat_filename)
    EnvStat = get_mode(EnvStat,codec_mode,'envelope_order','envelope_mode',...
                    'envelope_quantization_target_dB');
    if ~isstruct(EnvStat), error(' '), end
catch
    disp('Training data not found. Generating training data.')
    
    train_all
    
    generate_parameters
    extract_filenames


    load(envstat_filename)
    EnvStat = get_mode(EnvStat,codec_mode,'envelope_order','envelope_mode',...
                    'envelope_quantization_target_dB');
    
end

% Envelope pdf
if ~codec_mode.envelope_covariance_diagonal
    EnvStat.pdf = EnvStat.GMModel{codec_mode.envelope_mixture_order};
    for k=1:codec_mode.envelope_mixture_order
        EnvStat.iCov(:,:,k) = pinv(EnvStat.pdf.Sigma(:,:,k));
        for h=1:codec_mode.envelope_order
            EnvStat.CovDet(k,h) = det(EnvStat.pdf.Sigma(1:h,1:h,k));
        end
    end
end



% code random files
for fileix= randperm(filenum_test)
    
    x=readsph(d_test{fileix},[],-1,-1);
    winnum = floor((length(x)-window_length)/window_step)+1;

    xq = zeros(size(x));
    Xmat = zeros(speclen,winnum);
    Xqmat = Xmat;
    bits_env = zeros(winnum,1);
    for k=1:winnum
        ix = (k-1)*window_step + (1:window_length);
        xwin = x(ix).*hwin;
        
        X = MdctMtx*xwin;
        Xmat(:,k) = X;
        
        
        %% Envelope
        Env = EnvStat.EnvFn(X);
        Envq = (0.5+floor(Env/EnvStat.dq_env))*EnvStat.dq_env;
        Ql = floor(Env/EnvStat.dq_env)*EnvStat.dq_env;
        Qu = ceil(Env/EnvStat.dq_env)*EnvStat.dq_env;
        
        [bits_env(k),Envq] = envelope_coder(Ql,Qu,Envq,EnvStat,codec_mode,bits_per_frame);
    end
    
    clf
    subplot(211)
    surf(20*log10(abs(Xmat)),'EdgeColor','none')
    view(2)
    axis tight
    subplot(212)
    plot(bits_env)
    axis tight
    break
    waitforbuttonpress
    
end