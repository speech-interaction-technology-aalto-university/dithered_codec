function test_perceptual_model(paramlist);

SNRlevel_dB = 11;
disp('Test perceptual model for dithered coding experiments')
disp(['   Simulated quantization with SNR ' num2str(SNRlevel_dB) ' dB'])

SNR = 10^(SNRlevel_dB/20);


[codec_mode,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters();
[d_train,d_test,filenum_train,filenum_test] = extract_filenames(timit_path,temp_path);


if nargin > 0
    runtime_mode = runtime_configuration(codec_mode,paramlist);
end

% code random files
for fileix= 1%randperm(filenum_test)
    
    rng(1) % set random seed for reproducibility
    
    x=readsph(d_test{fileix},[],-1,-1);

            winnum = floor((length(x)-codec_mode.window_length)/codec_mode.window_step)+1;

        xq = zeros(size(x));
        Xmat = zeros(codec_mode.speclen,winnum);
        Xqmat = Xmat;
        for k=1:winnum
            ix = (k-1)*codec_mode.window_step + (1:codec_mode.window_length);
            xwin = x(ix).*codec_mode.hwin;

            X = codec_mode.MdctMtx*xwin;
            Xmat(:,k) = X;



            %% Perceptual model              
            W = perceptual_model(abs(X),codec_mode);        
            %W(:) = 1;
            
            Xw = X./W;
            
            %% Quantization simulation
            e = 2*rand(codec_mode.speclen,1) - 1; % quantization error
            e = e/std(e);

            Xwq = Xw + e*std(Xw)/SNR;
            
            


            %% resynthesis
            Xq = Xwq.*W; % restore envelope
            Xqmat(:,k) = Xq;
            
            xwinq = 2*codec_mode.MdctMtx'*Xq;
            ix = (k-1)*codec_mode.window_step + (1:codec_mode.window_length);
            xq(ix) = xq(ix) + xwinq.*codec_mode.hwin;

        end

       clf
        subplot(211)
        surf(20*log10(abs(Xmat)+0.001),'edgecolor','none')
        view(2)
        axis tight
        subplot(212)
        surf(20*log10(abs(Xqmat)+0.001),'edgecolor','none')
        view(2)
        axis tight        
        drawnow
     
    %disp(['Mean perceptual SNR ' num2str(mean(snr)) ' dB'])
    if 1
        soundsc([xq],codec_mode.sampling_frequency)
        pause(length(x)/codec_mode.sampling_frequency)
    elseif 0
        
        soundsc([x; xq],codec_mode.sampling_frequency)
        pause(2*length(x)/codec_mode.sampling_frequency)
    end    
    
end