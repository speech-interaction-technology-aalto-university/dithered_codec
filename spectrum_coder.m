function [Xwq,Xl,Xu] = spectrum_coder(Xw,bits_remaining,codec_mode,relative_bits_target,EnvStat)
% function [Xwq,Xl,Xu] = spectrum_coder(Xw,bits_remaining,codec_mode,relative_bits_target)
% Fixed rate coder for the spectrum

speclen = size(Xw,1);
Xwq = zeros(size(Xw));


%% Sort to ascending order
[relative_bits_target,sortix] = sort(relative_bits_target);
Xw = Xw(sortix);
if 0 %codec_mode.encoder_F0_model
    specpdf = EnvStat.spectral_postF0_pdf(sortix,3);
else
    specpdf = EnvStat.spectral_pdf(sortix,3);
end
specbits = EnvStat.spectral_bits(sortix);

%% initial bit distribution
quantize_range_left = 1;
quantize_range_right = length(Xw);
cumulative_bits = [0; cumsum(relative_bits_target)];
bit_offset = (bits_remaining - cumulative_bits(1+quantize_range_right))/speclen;

% remove negative bits
while (relative_bits_target(quantize_range_left) + bit_offset <= 0) && (quantize_range_left+1 < quantize_range_right)
    quantize_range_left = quantize_range_left + 1;
    bit_offset = (bits_remaining - cumulative_bits(1+quantize_range_right) + cumulative_bits(1+quantize_range_left-1))/(quantize_range_right - quantize_range_left + 1);
end

%% high-rate part
if codec_mode.dithering
    r = rand(speclen,1);
else
    r = zeros(speclen,1);
end
if codec_mode.quantizer_1bit
    bit_threshold = 1;
else
    bit_threshold = 0;
end
Xl = -100+Xwq; % minus "infinity"
Xu = +100+Xwq; % plus "infinity"
bitspec = Xwq+NaN;
while (relative_bits_target(quantize_range_right) + bit_offset > bit_threshold) ...
        && (quantize_range_left < quantize_range_right)
    
    % quantize
    %dq = 2.^-(relative_bits_target(quantize_range_right)+bit_offset-1.9427); % step size    
    dq = 2.^-(relative_bits_target(quantize_range_right)+bit_offset-specbits(quantize_range_right)); % step size    
    Xl(quantize_range_right) = dq.*(    - r(quantize_range_right) + floor(Xw(quantize_range_right)./dq + r(quantize_range_right)));
    Xu(quantize_range_right) = dq.*(1.0 - r(quantize_range_right) + floor(Xw(quantize_range_right)./dq + r(quantize_range_right)));
    switch codec_mode.spectrum_distribution
        case 'laplacian'
            Cl = laplacian_cdf(Xl(quantize_range_right));
            Cu = laplacian_cdf(Xu(quantize_range_right));            
        case 'logistic mixture'
            Cl =  1./(1 + exp(-(Xl(quantize_range_right) - specpdf(quantize_range_right).mu)./specpdf(quantize_range_right).s));
            Cu =  1./(1 + exp(-(Xu(quantize_range_right) - specpdf(quantize_range_right).mu)./specpdf(quantize_range_right).s));
            Cl = specpdf(quantize_range_right).proportion'*Cl;
            Cu = specpdf(quantize_range_right).proportion'*Cu;
    end

    bitspec(quantize_range_right) = -log2(Cu - Cl + eps);
    bits_remaining = bits_remaining - bitspec(quantize_range_right);    
    
    quantize_range_right = quantize_range_right - 1;
    bit_offset = (bits_remaining ...
        - cumulative_bits(1+quantize_range_right) ...
        + cumulative_bits(1+quantize_range_left))/(quantize_range_right - quantize_range_left + 1);    
    
    % check if we have to move left end to the right = if bit target at
    % left end is negative
    while (relative_bits_target(quantize_range_left) + bit_offset <= 0) && (quantize_range_left+1 < quantize_range_right)
        quantize_range_left = quantize_range_left + 1;
        bit_offset = (bits_remaining - cumulative_bits(1+quantize_range_right) + cumulative_bits(1+quantize_range_left-1))...
            /(quantize_range_right - quantize_range_left + 1);    
    end
    % check if we can move left end to the left = if bit target at
    % left end does not become negative
    while 1
        left_test = quantize_range_left - 1;
        if left_test < 1
            break
        end
        
        offset_test = (bits_remaining - cumulative_bits(1+quantize_range_right) + cumulative_bits(1+left_test-1))...
            /(quantize_range_right - left_test + 1); 
        
        if relative_bits_target(left_test) + offset_test > 0
            quantize_range_left = left_test;
            bit_offset = offset_test;
        else
            break
        end
    end
end

%% decode highrate
high_ix = (quantize_range_right+1):speclen;
switch codec_mode.decoder_mode
    case 'mean'
        switch codec_mode.spectrum_distribution
            case 'laplacian'
                Xwq(high_ix) = expectation_truncated_laplacian(Xl(high_ix),Xu(high_ix),0,1);
            case 'logistic mixture'
                Xwq(high_ix) = expectation_truncated_logistic_mixture(Xl(high_ix),Xu(high_ix),specpdf(high_ix));
        end
    case 'mid'
        Xwq(high_ix) = 0.5*(Xl(high_ix) + Xu(high_ix));
    case 'var'
        switch codec_mode.spectrum_distribution
            case 'laplacian'
                Xwq(high_ix) = expectation2_truncated_laplacian(Xl(high_ix),Xu(high_ix),0,1);
            otherwise
                error('Combination not supported')
        end
end


%% lowrate
if codec_mode.quantizer_1bit
    low_ix = quantize_range_left:quantize_range_right;
    Xwq(low_ix) = lowrate_spectrum_coder(Xw(low_ix),bits_remaining,codec_mode,relative_bits_target(low_ix)+bit_offset);    
    
    if 0
        Xwq(low_ix) = sqrt(pi/2)*(Xwq(low_ix).*EnvStat.sign_gain(low_ix));
    end
end


% noise filling for zeroed out bins, 6dB below standard deviation
if codec_mode.noise_fill
    ixzero= (1:(quantize_range_left-1))';
    Xwq(ixzero) = randn(size(ixzero)).*min(1,(2.^-(relative_bits_target(ixzero)+bit_offset))-1);
end

if 0
    clf
    plot(Xw)
    hold on
    plot(high_ix,Xwq(high_ix))
    plot(low_ix,Xwq(low_ix))
    
    waitforbuttonpress
end

Xwq(sortix) = Xwq;