clear



[codec_mode,runtime_mode,timit_path,temp_path,envstat_filename] = generate_parameters();
[d_train,d_test,filenum_train,filenum_test] = extract_filenames(timit_path,temp_path);

x=readsph(d_test{1},[],-1,-1);

winnum = floor((length(x)-codec_mode.window_length)/codec_mode.window_step)+1;

for k=1:winnum
    ix = (k-1)*codec_mode.window_step + (1:codec_mode.window_length);
    xwin = x(ix).*codec_mode.hwin;

    X = codec_mode.MdctMtx*xwin;
    X = abs(X);
    
    codec_mode.envelope_mode = 'sfb_uniform';
    codec_mode.envelope_order = 16;
    codec_mode.envelope_exponent = 2;
    codec_mode_sfb = generate_envelope_model(codec_mode);    
    X_sfb = codec_mode_sfb.iLogEnvFn(codec_mode_sfb.EnvFn(X));
    sfb_uni = codec_mode_sfb.EnvMtx;
    

    codec_mode.envelope_mode = 'mfcc';
    codec_mode.envelope_order = 16;
    codec_mode_mfcc = generate_envelope_model(codec_mode);    
    X_mfcc = codec_mode_mfcc.iLogEnvFn(codec_mode_mfcc.EnvFn(X));
    mfccmx = codec_mode_mfcc.EnvMtx;
    
    codec_mode.envelope_mode = 'mfcc2';
    codec_mode.envelope_order = 32;
    codec_mode_mfcc2 = generate_envelope_model(codec_mode);    
    X_mfcc2 = codec_mode_mfcc2.iLogEnvFn(codec_mode_mfcc2.EnvFn(X));
    mfccmx2 = codec_mode_mfcc2.EnvMtx;

    clf
    subplot(211)
    hold on
    plot((1:320)/320,sfb_uni','k','linewidth',2)
    plot((1:320)/320,1.2+mfccmx','k','linewidth',2)
    plot((1:320)/320,2.4+mfccmx2','k','linewidth',2)
    
    
    subplot(212)
    plot([20*log10(abs(X)) X_sfb X_mfcc X_mfcc2],    'linewidth',2)
    waitforbuttonpress
end